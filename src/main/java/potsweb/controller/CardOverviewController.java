package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CardOverviewController {

    @GetMapping("/cards_overview")
    public String card_over() {

        return "cards_overview";
    }
}
