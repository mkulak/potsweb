package potsweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import potsweb.domain.User;
import potsweb.repos.UserRepository;

@Controller
public class LoginController {


    @GetMapping("/login")
    public String init() {

        return "login";
    }

    @PostMapping("/login")
    public String login(
            @RequestParam String login,
            @RequestParam String password,
            Model model
    ) {


        return "redirect:/accounts_overview";
    }
}
