package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsActivityLogController {

    @GetMapping("/settings_activity_log")
    public String init() {

        return "settings_activity_log";
    }
}
