package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForPaymentTransitAccountController {

    @GetMapping("/foreign_payments_transit_account")
    public String init() {

        return "foreign_payments_transit_account";
    }
}
