package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForPaymentCrossborderController {

    @GetMapping("/foreign_payments_cross-border")
    public String init() {

        return "foreign_payments_cross-border";
    }
}
