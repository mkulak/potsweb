package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForBuyCurrencyController {

    @GetMapping("/foreign_buy_currency")
    public String init() {

        return "foreign_buy_currency";
    }
}
