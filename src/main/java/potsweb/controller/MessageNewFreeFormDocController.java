package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MessageNewFreeFormDocController {

    @GetMapping("/message_new_free_form_document")
    public String init() {

        return "message_new_free_form_document";
    }
}
