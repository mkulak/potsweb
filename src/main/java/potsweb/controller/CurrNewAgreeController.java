package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CurrNewAgreeController {

    @GetMapping("/currency_control_new_agreement")
    public String init() {

        return "currency_control_new_agreement";
    }
}
