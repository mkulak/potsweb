package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class OrderArchiveController {

    @GetMapping("/order_archive")
    public String init() {

        return "order_archive";
    }
}
