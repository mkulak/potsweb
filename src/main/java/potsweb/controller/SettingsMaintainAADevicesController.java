package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsMaintainAADevicesController {

    @GetMapping("/settings_maintain_aa_devices")
    public String init() {

        return "settings_maintain_aa_devices";
    }
}
