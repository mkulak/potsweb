package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsMyMenuController {

    @GetMapping("/settings_my_menu")
    public String init() {

        return "settings_my_menu";
    }
}
