package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TermDepoOverviewController {

    @GetMapping("/term_deposit_overview")
    public String init() {

        return "term_deposit_overview";
    }
}
