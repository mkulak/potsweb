package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CardStatementController {

    @GetMapping("/cards_statements")
    public String stat() {

        return "cards_statements";
    }
}
