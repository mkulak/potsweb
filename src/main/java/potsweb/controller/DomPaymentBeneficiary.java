package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DomPaymentBeneficiary {

    @GetMapping("/domestic_payments_beneficiary")
    public String init() {

        return "domestic_payments_beneficiary";
    }
}
