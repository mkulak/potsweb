package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UploadStartController {

    @GetMapping("/start_upload_payments")
    public String init() {

        return "start_upload_payments";
    }
}
