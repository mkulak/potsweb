package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AccStatementController {

    @GetMapping("/acc_statement")
    public String stat() {

        return "acc_statement";
    }
}
