package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForPaymentTemplateController {

    @GetMapping("/foreign_payments_template")
    public String init() {

        return "foreign_payments_template";
    }
}
