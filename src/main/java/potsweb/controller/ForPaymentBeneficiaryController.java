package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForPaymentBeneficiaryController {

    @GetMapping("/foreign_payments_beneficiary")
    public String init() {

        return "foreign_payments_beneficiary";
    }
}
