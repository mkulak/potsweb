package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CurrOverviewController {

    @GetMapping("/currency_control_overview")
    public String init() {

        return "currency_control_overview";
    }
}
