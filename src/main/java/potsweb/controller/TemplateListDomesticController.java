package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemplateListDomesticController {

    @GetMapping("/templates_list_domestic")
    public String init() {

        return "templates_list_domestic";
    }
}
