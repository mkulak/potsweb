package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsNotificationSignatureController {

    @GetMapping("/settings_notification_signature")
    public String init() {

        return "settings_notification_signature";
    }
}
