package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DomPaymentTemplateController {

    @GetMapping("/domestic_payments_template")
    public String init() {

        return "domestic_payments_template";
    }
}
