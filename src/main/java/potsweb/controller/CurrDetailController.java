package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CurrDetailController {

    @GetMapping("/currency_control_detail")
    public String init() {

        return "currency_control_detail";
    }
}
