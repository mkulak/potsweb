package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MessageOutboxController {

    @GetMapping("/message_outbox")
    public String init() {

        return "message_outbox";
    }
}
