package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CardCreateController {

    @GetMapping("/cards_create")
    public String create() {

        return "cards_create";
    }
}
