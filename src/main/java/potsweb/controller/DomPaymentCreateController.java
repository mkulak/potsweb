package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DomPaymentCreateController {

    @GetMapping("/domestic_payments_create")
    public String init() {

        return "domestic_payments_create";
    }
}
