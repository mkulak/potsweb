package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsDuplicatePaymentController {

    @GetMapping("/settings_duplicate_payment")
    public String init() {

        return "settings_duplicate_payment";
    }
}
