package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsNotificationOverviewController {

    @GetMapping("/settings_notification_overview")
    public String init() {

        return "settings_notification_overview";
    }
}
