package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MessageInboxController {

    @GetMapping("/message_inbox")
    public String init() {

        return "message_inbox";
    }
}
