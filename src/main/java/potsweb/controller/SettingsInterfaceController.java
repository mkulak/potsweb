package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsInterfaceController {

    @GetMapping("/settings_interface")
    public String init() {

        return "settings_interface";
    }
}
