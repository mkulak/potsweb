package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsPasswordChangeController {

    @GetMapping("/settings_password_change")
    public String init() {

        return "settings_password_change";
    }
}
