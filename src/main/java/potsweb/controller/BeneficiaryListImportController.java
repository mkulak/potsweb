package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BeneficiaryListImportController {

    @GetMapping("/beneficiary_list_partners_import")
    public String init() {

        return "beneficiary_list_partners_import";
    }
}
