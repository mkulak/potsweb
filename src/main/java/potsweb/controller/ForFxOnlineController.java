package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForFxOnlineController {

    @GetMapping("/foreign_fx_online")
    public String init() {

        return "foreign_fx_online";
    }
}
