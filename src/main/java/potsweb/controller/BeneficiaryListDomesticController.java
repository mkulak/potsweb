package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BeneficiaryListDomesticController {

    @GetMapping("/beneficiary_list_domestic")
    public String init() {

        return "beneficiary_list_domestic";
    }
}
