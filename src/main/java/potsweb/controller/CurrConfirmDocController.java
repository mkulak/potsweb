package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CurrConfirmDocController {

    @GetMapping("/currency_control_confirm_doc")
    public String init() {

        return "currency_control_confirm_doc";
    }
}
