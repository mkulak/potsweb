package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DomPaymentOverviewController {

    @GetMapping("/domestic_payments_overview")
    public String init() {

        return "domestic_payments_overview";
    }
}
