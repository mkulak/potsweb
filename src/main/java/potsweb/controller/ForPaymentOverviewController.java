package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForPaymentOverviewController {

    @GetMapping("/foreign_payments_overview")
    public String init() {

        return "foreign_payments_overview";
    }
}
