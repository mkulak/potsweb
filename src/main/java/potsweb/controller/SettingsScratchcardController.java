package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsScratchcardController {

    @GetMapping("/settings_scratchcard")
    public String init() {

        return "settings_scratchcard";
    }
}
