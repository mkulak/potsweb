package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SalaryInboxController {

    @GetMapping("/salary_inbox")
    public String init() {

        return "salary_inbox";
    }
}
