package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsGeneralController {

    @GetMapping("/settings_general")
    public String init() {

        return "settings_general";
    }
}
