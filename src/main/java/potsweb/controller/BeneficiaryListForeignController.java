package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BeneficiaryListForeignController {

    @GetMapping("/beneficiary_list_foreign")
    public String init() {

        return "beneficiary_list_foreign";
    }
}
