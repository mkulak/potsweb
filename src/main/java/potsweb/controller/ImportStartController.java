package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ImportStartController {

    @GetMapping("/start_import_payments")
    public String init() {

        return "start_import_payments";
    }
}
