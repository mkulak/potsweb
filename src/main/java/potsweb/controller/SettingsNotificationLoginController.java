package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SettingsNotificationLoginController {

    @GetMapping("/settings_notification_login")
    public String init() {

        return "settings_notification_login";
    }
}
