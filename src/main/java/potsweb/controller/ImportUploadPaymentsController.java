package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ImportUploadPaymentsController {

    @GetMapping("/import_upload_payments")
    public String init() {

        return "import_upload_payments";
    }
}
