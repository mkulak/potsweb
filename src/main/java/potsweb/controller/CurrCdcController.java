package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CurrCdcController {

    @GetMapping("/currency_control_cdc")
    public String init() {

        return "currency_control_cdc";
    }
}
