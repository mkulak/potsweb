package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForTransitAccOrderController {

    @GetMapping("/foreign_transit_account_order")
    public String init() {

        return "foreign_transit_account_order";
    }
}

