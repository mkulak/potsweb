package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TemplateListForeignController {

    @GetMapping("/templates_list_foreign")
    public String init() {

        return "templates_list_foreign";
    }
}
