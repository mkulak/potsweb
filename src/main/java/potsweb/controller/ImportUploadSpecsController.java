package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ImportUploadSpecsController {

    @GetMapping("/import_upload_specifications")
    public String init() {

        return "import_upload_specifications";
    }
}
