package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TermDepoNewAgreeController {

    @GetMapping("/term_deposit_new_agreement")
    public String init() {

        return "term_deposit_new_agreement";
    }
}
