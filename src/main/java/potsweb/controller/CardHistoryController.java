package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CardHistoryController {

    @GetMapping("/cards_history")
    public String card_history() {

        return "cards_history";
    }
}
