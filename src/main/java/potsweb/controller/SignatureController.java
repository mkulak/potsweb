package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SignatureController {

    @GetMapping("/signature")
    public String init() {

        return "signature";
    }
}
