package potsweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AccHistoryController {

    @GetMapping("/acc_history")
    public String history() {

        return "acc_history";
    }
}
