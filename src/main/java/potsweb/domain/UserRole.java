package potsweb.domain;

public enum UserRole {

    USER,
    ADMIN
}
