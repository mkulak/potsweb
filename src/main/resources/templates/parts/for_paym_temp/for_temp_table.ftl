<#macro for_temp_table>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"></th>
        <th scope="col"><small><b>Template Name</b></small></th>
        <th scope="col"><small><b>Account<br>Account name</b></small></th>
        <th scope="col"><small><b>Curr</b></small></th>
        <th scope="col"><small><b>Partner acc./IBAN<br>Patner Name</b></small></th>
        <th scope="col"><small><b>Bankcode/BIC<br>Bankname</b></small></th>
        <th scope="col"><small><b>Availability<br>Client</b></small></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

</#macro>