<#macro arch>
<ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link text-dark" href="/order_archive">
            <small>
                <span class="fas fa-angle-right text-danger" id="arch"></span>
                <strong>ORDER ARCHIVE</strong>
            </small>
        </a>
    </li>
</ul>
</#macro>