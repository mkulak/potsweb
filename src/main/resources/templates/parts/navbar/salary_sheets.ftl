<#macro salary>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#salary_tree" href="" onclick="salary()">
        <small>
            <span class="fas fa-angle-right text-danger" id="salary"></span>
            <strong>SALARY SHEETS</strong>
        </small>
    </a>
    <div id="salary_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/salary_inbox">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="sal"></span>
                        <strong>Salary Inbox</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled5 = false;
    function salary(){
        if(!toggled5){
            toggled5 = true;
            document.getElementById("salary").className ="fas fa-angle-down text-danger";
        } else {
            toggled5 = false;
            document.getElementById("salary").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>