<#macro term>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#term_tree" href="#" onclick="depo()">
        <small>
            <span class="fas fa-angle-right text-danger" id="depo"></span>
            <strong>TERM DEPOSITS</strong>
        </small>
    </a>
    <div id="term_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/term_deposit_overview">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="depo_over"></span>
                        <strong>Term Deposits Overview</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/term_deposit_new_agreement">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="new_agree"></span>
                        <strong>Accounts Overviews</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled6 = false;
    function depo(){
        if(!toggled6){
            toggled6 = true;
            document.getElementById("depo").className ="fas fa-angle-down text-danger";
        } else {
            toggled6 = false;
            document.getElementById("depo").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>