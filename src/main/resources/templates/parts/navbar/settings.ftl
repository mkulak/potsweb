<#macro set>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#set_tree" href="" onclick="settings()">
        <small>
            <span class="fas fa-angle-right text-danger" id="set"></span>
            <strong>SETTINGS</strong>
        </small>
    </a>
    <div id="set_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_my_menu">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="menu"></span>
                        <strong>My Menu</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_activity_log">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="log"></span>
                        <strong>Activity Log</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_general">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="gen"></span>
                        <strong>General</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_interface">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="inter"></span>
                        <strong>Interface Settings</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_maintain_aa_devices">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="aa"></span>
                        <strong>Maintain A&A Devices</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_password_change">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="password"></span>
                        <strong>Password Change</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" data-toggle="collapse" data-target="#notif_tree" href="/settings_notification_overview">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="password"></span>
                        <strong>Notifications</strong>
                    </small>
                </a>
                <div id="notif_tree" class="collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/settings_notification_login">
                                <small>Login</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/settings_notification_signature">
                                <small>Signature</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_duplicate_payment">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="dupl"></span>
                        <strong>Duplicate Payment Settings</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/settings_scratchcard">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="scratch"></span>
                        <strong>ScratchCard</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled8 = false;
    function settings(){
        if(!toggled8){
            toggled8 = true;
            document.getElementById("set").className ="fas fa-angle-down text-danger";
        } else {
            toggled8 = false;
            document.getElementById("set").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>