<#macro currency>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#curr_tree" href="" onclick="control()">
        <small>
            <span class="fas fa-angle-right text-danger" id="control"></span>
            <strong>CURRENCY CONTROL</strong>
        </small>
    </a>
    <div id="curr_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/currency_control_overview">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Contract & Currency Operations Overview</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/currency_control_new_agreement">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="new"></span>
                        <strong>Send new Agreement to Bank</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/currency_control_detail">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="detail"></span>
                        <strong>Create Detail for Currency Control</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/currency_control_confirm_doc">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="confirm"></span>
                        <strong>Create Confirming Document</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/currency_control_cdc">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="cdc"></span>
                        <strong>Create CDC</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled4 = false;
    function control(){
        if(!toggled4){
            toggled4 = true;
            document.getElementById("control").className ="fas fa-angle-down text-danger";
        } else {
            toggled4 = false;
            document.getElementById("control").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>