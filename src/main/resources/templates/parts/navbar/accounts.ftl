<#macro accs>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#tree1" href="" onclick="acc()">
        <small>
            <span class="fas fa-angle-right text-danger" id="general"></span>
            <strong>ACCOUNTS</strong>
        </small>
    </a>
    <div id="tree1" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link ml-2 text-dark" href="/acc_overview">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Accounts Overviews</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link ml-2 text-dark" href="/acc_statement">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="statement"></span>
                        <strong>Statements</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/acc_history">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="history"></span>
                        <strong>History</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled = false;
    function acc(){
        if(!toggled){
            toggled = true;
            document.getElementById("general").className ="fas fa-angle-down text-danger";
        } else {
            toggled = false;
            document.getElementById("general").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>