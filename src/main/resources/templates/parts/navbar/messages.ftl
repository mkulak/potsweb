<#macro mess>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#mess_tree" href="" onclick="messages()">
        <small>
            <span class="fas fa-angle-right text-danger" id="messages"></span>
            <strong>MESSAGES</strong>
        </small>
    </a>
    <div id="mess_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/message_new_free_form_document">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="free"></span>
                        <strong>New Free-Format Document</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/message_inbox">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="inbox"></span>
                        <strong>Inbox</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/message_outbox">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="outbox"></span>
                        <strong>Outbox</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled7 = false;
    function messages(){
        if(!toggled7){
            toggled7 = true;
            document.getElementById("messages").className ="fas fa-angle-down text-danger";
        } else {
            toggled7 = false;
            document.getElementById("messages").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>