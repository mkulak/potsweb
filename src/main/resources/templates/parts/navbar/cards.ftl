<#macro crds>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#card_tree" href="" onclick="card()">
        <small>
            <span class="fas fa-angle-right text-danger" id="cards"></span>
            <strong>CARDS</strong>
        </small>
    </a>
    <div id="card_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/cards_overview">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Cards Overviews</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/cards_history">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Card History</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/cards_statements">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Card Statements</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/cards_requests">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Cards Requests</strong>
                    </small>
                </a>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" href="/cards_create">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Create card request</strong>
                    </small>
                </a>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled = false;
    function card(){
        if(!toggled){
            toggled = true;
            document.getElementById("cards").className ="fas fa-angle-down text-danger";
        } else {
            toggled = false;
            document.getElementById("cards").className = "fas fa-angle-right text-danger";
        }
    }
</script>
</#macro>