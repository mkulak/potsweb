<#macro paym>
<li class="nav-item">
    <a class="nav-link text-dark" data-toggle="collapse" data-target="#payment_tree" href="" onclick="paym()">
        <small>
            <span class="fas fa-angle-right text-danger" id="payments"></span>
            <strong>PAYMENTS</strong>
        </small>
    </a>
    <div id="payment_tree" class="collapse">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" data-toggle="collapse" data-target="#domestic_tree" href="" onclick="over()">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Domestic</strong>
                    </small>
                </a>
                <div id="domestic_tree" class="collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/domestic_payments_overview">
                                <small>Domestic Payments Overview</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/domestic_payments_create">
                                <small>Create a Payment</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/domestic_payments_template">
                                <small>Templates</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/domestic_payments_beneficiary">
                                <small>List of Beneficiaries</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" data-toggle="collapse" data-target="#foreign_tree" href="#">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Foreign</strong>
                    </small>
                </a>
                <div id="foreign_tree" class="collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_payments_overview">
                                <small>Payments Overview</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_payments_cross-border">
                                <small>Cross-border Payment</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_payments_transit_account">
                                <small>Payment form Transit Account</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_buy_currency">
                                <small>Buy Foreign Currency</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_sell_currency">
                                <small>Sell Foreign Currency</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_fx_online">
                                <small>FX Online</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_transit_account_order">
                                <small>Transit Account Order</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_payments_template">
                                <small>Templates</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/foreign_payments_beneficiary">
                                <small>Beneficiaries</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" data-toggle="collapse" data-target="#import_tree" href="">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>Import & Uploads</strong>
                    </small>
                </a>
                <div id="import_tree" class="collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/import_upload_payments">
                                <small>Import & Uploads</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/start_import_payments">
                                <small>Start Import</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/start_upload_payments">
                                <small>Start Upload</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/import_upload_specifications">
                                <small>Import/Upload - Format Specification</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" data-toggle="collapse" data-target="#beneficiaries_tree" href="">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>List of Beneficiaries</strong>
                    </small>
                </a>
                <div id="beneficiaries_tree" class="collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/beneficiary_list_domestic">
                                <small>Domestic</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/beneficiary_list_foreign">
                                <small>Foreign</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/beneficiary_list_partners_import">
                                <small>Import partners</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-dark ml-2" data-toggle="collapse" data-target="#templates_tree" href="">
                    <small>
                        <span class="fas fa-angle-right text-secondary" id="overview"></span>
                        <strong>List of Templates</strong>
                    </small>
                </a>
                <div id="templates_tree" class="collapse">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/templates_list_domestic">
                                <small>Domestic</small>
                            </a>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link text-dark ml-4" href="/templates_list_foreign">
                                <small>Foreign</small>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</li>
<script type="text/javascript">
    var toggled1 = false;
    function paym(){
        if(!toggled1){
            toggled1 = true;
            document.getElementById("payments").className ="fas fa-angle-down text-danger";
        } else {
            toggled1 = false;
            document.getElementById("payments").className = "fas fa-angle-right text-danger";
        }
    }
</script>
<script type="text/javascript">
    var toggled2 = false;
    function over(){
        if(!toggled2){
            toggled2 = true;
            document.getElementById("overview").className ="fas fa-angle-down text-secondary";
        } else {
            toggled2 = false;
            document.getElementById("overview").className = "fas fa-angle-right text-secondary";
        }
    }
</script>
</#macro>