<#import "accounts.ftl" as a>
<#import "cards.ftl" as c>
<#import "payments.ftl" as p>
<#import "currency_control.ftl" as cur>
<#import "salary_sheets.ftl" as sal>
<#import "term_deposits.ftl" as t>
<#import "order_archive.ftl" as o>
<#import "signature.ftl" as s>
<#import "messages.ftl" as m>
<#import "settings.ftl" as set>

<#macro navbar>
<nav class="navbar shadow p-3 mb-5" style="border-radius: 30px">
    <ul class="navbar-nav">
        <@a.accs/>
        <@c.crds/>
        <@p.paym/>
        <@cur.currency/>
        <@sal.salary/>
        <@t.term/>
        <@s.sign/>
        <@o.arch/>
        <@m.mess/>
        <@set.set/>
    </ul>
</nav>
</#macro>
