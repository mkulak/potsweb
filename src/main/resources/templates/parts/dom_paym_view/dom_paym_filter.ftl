<#macro dom_paym_filter>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner Name</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex p-2 text-secondary pl-3"><small><strong>Partner BIN(IIN)</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner Account</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex p-2 text-secondary pl-3"><small><strong>Partner KPP</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount from</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex p-2 text-secondary pl-3"><small><strong>Amount to</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transaction Details</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex p-2 text-secondary pl-3"><small><strong>Document No.</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
</div>
</#macro>