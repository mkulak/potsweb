<#macro dom_paym_table>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"><small><b>Status</b></small></th>
        <th scope="col"><small><b>Order type</b></small></th>
        <th scope="col"><small><b>Account<br>Beneficiary INN,Beneficiary Name<br>Payment details</b></small></th>
        <th scope="col"><small><b>Date</b></small></th>
        <th scope="col"><small><b>No</b></small></th>
        <th scope="col"><small><b>Amount</b></small></th>
        <th scope="col"><small><b>Curr</b></small></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</#macro>