<#macro upload_paym_table>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"><small><b>S</b></small></th>
        <th scope="col"><small><b>Status</b></small></th>
        <th scope="col"><small><b>Pay.Type<br>Changed</b></small></th>
        <th scope="col"><small><b>Filename<br>Description</b></small></th>
        <th scope="col"><small><b>Type<br>Structure</b></small></th>
        <th scope="col"><small><b>Timestamp<br>from / to</b></small></th>
        <th scope="col"><small><b>Checksum<br>from / to<br>Digest</b></small></th>
        <th scope="col"><small><b>Errors</b></small></th>
        <th scope="col"><small><b># To sign<br># Orders</b></small></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

</#macro>