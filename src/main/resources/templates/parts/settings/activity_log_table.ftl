<#macro table>
<table class="table-sm table-striped col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"><small><b>Begin<br>Date/Time</b></small></th>
        <th scope="col"><small><b>Customer Name<br>Account</b></small></th>
        <th scope="col"><small><b>Action Type I<br>Action Type II</b></small></th>
        <th scope="col"><small><b>Order Type<br>Amount<br>Curr.</b></small></th>
        <th scope="col"><small><b>Status<br>Result</b></small></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</#macro>