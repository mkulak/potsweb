<#import "../awesome/search.ftl" as se>

<#macro for_paym_create>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>BIC</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="d-flex justify-content-end p-2 fa ml-3"><@se.search "5"/></div>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank Country Code</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="d-flex justify-content-end p-2 fa ml-3"><@se.search "7"/></div>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank City</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1 mb-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank Address</strong></small></div>
    <textarea class="custom-select-sm col-md-8 form-control"></textarea>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Corresp. account</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1 mb-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Branch name and other requisites</strong></small></div>
    <textarea class="custom-select-sm col-md-8 form-control"></textarea>
</div>
</#macro>