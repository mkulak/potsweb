<#macro for_paym_filter>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner Name</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Account No. / IBAN</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex justify-content-center p-2 text-secondary"><small><strong>SWIFT / BIC</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount from / to</strong></small></div>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
    <div class="col-md-1 d-flex p-2"></div>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
    <div class="col-md-2 d-flex justify-content-around p-2 text-secondary"><small><strong>Currency</strong></small></div>
    <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transaction Details</strong></small></div>
    <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
    <div class="col-md-2 d-flex justify-content-around p-2 text-secondary"><small><strong>Doc num.</strong></small></div>
    <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
</div>
</#macro>