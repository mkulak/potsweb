<#macro credit_card>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"><small><b>Card no.<br>Main card</b></small></th>
        <th scope="col"><small><b>Status<br>Type</b></small></th>
        <th scope="col"><small><b>Card Product</b></small></th>
        <th scope="col"><small><b>Valid to</b></small></th>
        <th scope="col"><small><b>Card Holder<br>Full name</b></small></th>
        <th scope="col"><small><b>Actual Limit</b></small></th>
        <th scope="col"><small><b>Curr</b></small></th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</#macro>