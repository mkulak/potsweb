<#macro credit_card>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transactions for</strong></small></div>
    <select class="custom-select-sm col-md-7 bg-white d-flex p-2" id="days">
        <option value="default">-------</option>
        <option value="acc1">Acc-1</option>
        <option value="acc2">Acc-2</option>
    </select>
    <div class="col-md-2 d-flex p-2"><small>of the last 15day(s)</small></div>
</div>
</#macro>