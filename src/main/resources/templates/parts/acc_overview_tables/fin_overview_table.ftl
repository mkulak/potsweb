<#macro fin_overview>
<table class="table-sm table-striped table-hover col-md-12 shadow" id="sort" >
    <thead class="table-primary">
    <tr>
        <th class="th-sm" scope="col"></th>
        <th class="th-sm" scope="col"><small><b>Account Number<br>Account Number (internal)</b></small></th>
        <th class="th-sm" scope="col"><small><b>Account Owner<br>Account Type</b></small></th>
        <th class="th-sm" scope="col"><small><b>Date & Time of available<br>balance</b></small></th>
        <th class="th-sm" scope="col"><small><b>Available balance<br>Opening balance</b></small></th>
        <th class="th-sm" scope="col"><small><b>Curr</b></small></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td class="td-sm"><small>1324214<br>454545m4545</small></td>
        <td class="td-sm"><small>124321432143<br>Business</small></td>
        <td class="td-sm"><small>14.05.2018<br>0.40</small></td>
        <td class="td-sm"><small>12324.94<br>122494403.84</small></td>
        <td class="td-sm"><small>UAH</small></td>
    </tr>
    <tr>
        <td></td>
        <td><small>1324214<br>454545m4545</small></td>
        <td><small>124321432143<br>Business</small></td>
        <td><small>14.05.2018<br>0.40</small></td>
        <td><small>12324.94<br>122494403.84</small></td>
        <td><small>UAH</small></td>
    </tr>
    </tbody>
</table>



<script>

    jQuery(document).ready(function($) {
        $('#sort').DataTable();
    });


</script>



</#macro>