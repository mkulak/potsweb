<#macro curr_acc>
    <table class="table-sm table-striped table-hover col-md-12 shadow">
        <thead class="table-primary">
        <tr>
            <th scope="col"><small><b>Account Number<br>Account Number (internal)</b></small></th>
            <th scope="col"><small><b>Account Owner<br>Account Type</b></small></th>
            <th scope="col"><small><b>Date & Time of available<br>balance</b></small></th>
            <th scope="col"><small><b>Available balance<br>Opening balance</b></small></th>
            <th scope="col"><small><b>Curr</b></small></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</#macro>