<#macro filter>

<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Trans. Amount from / to</strong></small></div>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2 ml-3"/>
    <select class="custom-select-sm col-md-2 bg-white d-flex p-2 ml-3" id="curr">
        <option value="all">All currencies</option>
        <option value="curr1">Curr-1</option>
    </select>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Direction</strong></small></div>
    <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="dir">
        <option value="all">All</option>
        <option value="1">Incoming</option>
        <option value="2">Outgoing</option>
    </select>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Save Group Name</strong></small></div>
    <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="name">
        <option selected>Select Group Name</option>
    </select>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank Code/SWIFT</strong></small></div>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner name</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner account / IBAN</strong></small></div>
    <input type="text" class="form-control-sm col-md-4 d-flex p-2"/>
    <div class="col-md-2 d-flex p-2 text-secondary"><small><strong>Partner INN</strong></small></div>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary mb-1"><small><strong>Payment details</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
</#macro>