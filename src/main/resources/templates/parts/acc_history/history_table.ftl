<#macro stat_table>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"></th>
        <th scope="col"><small><b>Value Date<br>No<br>Reference</b></small></th>
        <th scope="col"><small><b>Trans. Amount<br>CCY</b></small></th>
        <th scope="col"><small><b>C/D</b></small></th>
        <th scope="col"><small><b>INN / Account<br>Partner Name</b></small></th>
        <th scope="col"><small><b>Payment detail</b></small></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</#macro>