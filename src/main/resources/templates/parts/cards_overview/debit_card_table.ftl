<#macro debit_card>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead>
    <tr class="table-primary">
        <th scope="col"><small><b>Card no.</b></small></th>
        <th scope="col"><small><b>Status<br>Type</b></small></th>
        <th scope="col"><small><b>Card<br>Product</b></small></th>
        <th scope="col"><small><b>Valid to</b></small></th>
        <th scope="col"><small><b>Card Holder<br>Full name</b></small></th>
        <th scope="col"><small><b>Actual Limit</b></small></th>
        <th scope="col"><small><b>Curr</b></small></th>
        <th scope="col"></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</#macro>