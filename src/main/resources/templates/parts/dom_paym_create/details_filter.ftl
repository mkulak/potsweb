<#import "../awesome/info.ftl" as info>
<#import "../awesome/search.ftl" as se>

<#macro details_filter>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary mt-1"><small><strong>VAT Calc Type</strong></small></div>
    <select class="custom-select-sm col-md-3 bg-white d-flex p-2 mt-1">
        <option selected>Vat is not specified</option>
        <option value="vat1">Vat-1</option>
    </select>
    <input class="d-flex p-2 ml-3 mt-2" type="checkbox" aria-label="Checkbox for following text input"/>
    <div class="d-flex p-2"><small>Set as default</small></div>
    <div class="d-flex justify-content-end p-2 fa">
        <@info.fav "3"/>
    </div>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>VO Code</strong></small></div>
    <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
    <div class="d-flex justify-content-end p-2 fa">
        <@se.search "4"/>
    </div>
    <div class="ml-4">
        <a href="#" class="btn btn-outline-danger" style="border-radius: 8px" title="Place VO based on selection"><small><strong>Place VO ></strong></small></a>
    </div>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Copy of execution</strong></small></div>
    <input class="d-flex p-2 mt-2" type="checkbox" aria-label="Checkbox for following text input"/>
</div>
</#macro>