<#import "../awesome/search.ftl" as se>
<#import "../awesome/house.ftl" as ho>

<#macro benef_filter>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary INN</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex p-2 text-secondary pl-2"><small><strong>Beneficiary KPP</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="mt-1 ml-2"><@ho.home "1"/></div>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Counter parties check</strong></small></div>
    <button type="button" class="btn-sm col-md-1">Check</button>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>BIC</strong></small></div>
    <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
    <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
    <div class="mt-1"><@se.search "5"/></div>
    <div class="col-md-2 d-flex p-2 text-secondary ml-4"><small><strong>Corr Account</strong></small></div>
    <div class="col-md-2 d-flex p-2"><small>Value</small></div>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Bank Name</strong></small></div>
    <div class="col-md-3 d-flex p-2"><small>Value</small></div>
</div>
</#macro>