<#macro benef_filter>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Account</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <div class="col-md-2 d-flex justify-content-center p-2 text-secondary"><small><strong>INN</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Name</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
</#macro>