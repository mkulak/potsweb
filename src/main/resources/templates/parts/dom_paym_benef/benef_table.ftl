<#macro benef_table>
<table class="table-sm table-striped table-hover col-md-12 shadow">
    <thead class="table-primary">
    <tr>
        <th scope="col"></th>
        <th scope="col"><small><b>Name of the Partner</b></small></th>
        <th scope="col"><small><b>Partner Account<br>Partner Name</b></small></th>
        <th scope="col"><small><b>Bankcode<br>Bankname</b></small></th>
        <th scope="col"><small><b>Availability<br>Client</b></small></th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
</#macro>