<#macro card_filter>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transaction Amount from/to</strong></small></div>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
    <input type="text" class="form-control-sm col-md-3 d-flex p-2 ml-3"/>
</div>
<div class="row col-md-12 ml-1">
    <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transactions Details</strong></small></div>
    <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
</div>
</#macro>