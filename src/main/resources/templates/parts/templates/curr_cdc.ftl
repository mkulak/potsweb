<#macro curr_cdc>
<div class="col-md-12 shadow">
    <h4 class="text-danger">CONFIRMING DOCUMENT CERTIFICATE</h4>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order status</div>
        <div class="col-md-6 d-flex p-2 mt-1">New</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
        <div class="col-md-6 d-flex p-2 mt-1">Confirming Document Certificate</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Document no.</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
        <div class="col-md-2 d-flex p-2 mt-1">Document Date</div>
        <div class="input-group-sm col-md-2 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">By unique number contr.</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Client</div>
        <select class="custom-select-sm col-md-8 mt-2">
            <option selected>Select a client</option>
            <option value="1">Cli1</option>
            <option value="2">Cli2</option>
            <option value="3">Cli3</option>
        </select>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Name</div>
        <div class="col-md-6 d-flex p-2 mt-1">АБДУЛЛАЕВ АЛЬБЕРТ АЛЕКСЕЕВИЧ</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">State registration num</div>
        <div class="col-md-6 d-flex p-2 mt-1">312774624900109</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">INN</div>
        <div class="col-md-6 d-flex p-2 mt-1">772334920004</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Branch office</div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Select branch office</option>
            <option value="1">Bra1</option>
        </select>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Sender official</div>
        <div class="input-group-sm col-md-5 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        <div class="col-md-3 d-flex p-2 mt-1">Set as default</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Phone official</div>
        <div class="input-group-sm col-md-5 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        <div class="col-md-3 d-flex p-2 mt-1">Set as default</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-4 d-flex p-2 mt-1 text-danger"><strong>Confirmation Documents</strong></div>
    </div>
    <table class="table-sm table-striped col-md-12">
        <thead class="table-primary">
        <tr>
            <th scope="col"><small><b>Number</b></small></th>
            <th scope="col"><small><b>Confirming Doc. No.<br>Confirming Doc. Date</b></small></th>
            <th scope="col"><small><b>Doc. Type</b></small></th>
            <th scope="col"><small><b>Amount Doc. Curr.<br>Amount Contr. Curr.</b></small></th>
            <th scope="col"><small><b>Doc. Curr.<br>Contr. Curr.</b></small></th>
            <th scope="col"><small><b>Correction</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Add document >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Create document >"/>
        </form>
    </div>

    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Sign >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Reuse >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Print CBR >"/>
        </form>
        <form class="mt-2 ml-3" action="/currency_control_overview">
            <input class="text-danger border-danger" type="submit" value="Exit >"/>
        </form>
    </div>

</div>
</#macro>