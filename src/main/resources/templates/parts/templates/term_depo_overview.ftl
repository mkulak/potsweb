<#macro term_depo_over>
<div class="col-md-12 shadow">
    <h4 class="text-danger">TERM DEPOSIT OVERVIEW</h4>
    <div class="col-md-12 shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
            <select class="custom-select-sm col-md-3 mt-2">
                <option value="1">All</option>
                <option value="2">Term Deposit</option>
                <option value="3">Term Deposit Cancelation</option>
            </select>
            <div class="col-md-3 d-flex justify-content-center p-2 mt-1">Status</div>
            <select class="custom-select-sm col-md-3 mt-2">
                <option value="1">All</option>
                <option value="2">To sign</option>
                <option value="3">Signed</option>
                <option value="2">Accepted</option>
                <option value="3">Booked</option>
                <option value="3">Rejected</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Company</div>
            <select class="custom-select-sm col-md-9 mt-2">
                <option value="1">All</option>
                <option value="2">Comp1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">From Account</div>
            <select class="custom-select-sm col-md-9 mt-2">
                <option value="1">All</option>
                <option value="2">Acc1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Doc. date from/to</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="col-md-2 d-flex p-2 mt-1">Last days</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <select class="custom-select-sm col-md-2 mt-2">
                <option value="1">15</option>
                <option value="2">1</option>
                <option value="2">2</option>
                <option value="2">3</option>
                <option value="2">4</option>
                <option value="2">5</option>
                <option value="2">6</option>
                <option value="2">7</option>
                <option value="2">8</option>
                <option value="2">9</option>
                <option value="2">10</option>
                <option value="2">20</option>
                <option value="2">30</option>
            </select>
        </div>
        <div class="mt-1 col-md-12">
            <a class="btn btn-danger" data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="filter">
                Show extended filter
            </a>
            <div class="collapse mt-1" id="filter">
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Maturity date from/to</div>
                    <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                        <input type="text" class="form-control">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                    <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                        <input type="text" class="form-control">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Amount from/to</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-2 d-flex p-2 mt-1">Currency</div>
                    <select class="custom-select-sm col-md-1 mt-2">
                        <option value="1">All</option>
                        <option value="1">USD</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <form class="mt-2 ml-3" action="#">
                <input class="text-danger border-danger" type="submit" value="Select >"/>
            </form>
            <form class="mt-2 ml-3" action="#">
                <input class="text-danger border-danger mb-2" type="submit" value="Clear >"/>
            </form>
        </div>
    </div>

    <table class="table-sm table-striped col-md-12">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Status</b></small></th>
            <th scope="col"><small><b>Order type</b></small></th>
            <th scope="col"><small><b>from Account</b></small></th>
            <th scope="col"><small><b>Doc.<br>Date</b></small></th>
            <th scope="col"><small><b>No</b></small></th>
            <th scope="col"><small><b>Startdate<br><br>Mat.Date</b></small></th>
            <th scope="col"><small><b>Interest Rate</b></small></th>
            <th scope="col"><small><b>Amount</b></small></th>
            <th scope="col"><small><b>Curr</b></small></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>


    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Create >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Delete >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Cancel deposit >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Print >"/>
        </form>
    </div>

</div>

</#macro>