<#macro mess_inbox>
<div class="col-md-12 shadow">
    <h4 class="text-danger"><strong>INBOX</strong></h4>
    <div class="shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Topic</div>
            <select class="custom-select-sm col-md-8 d-flex p-2 mt-1">
                <option value="1">All</option>
                <option value="2">General</option>
                <option value="3">Payments Role</option>
                <option value="1">-</option>
                <option value="1">Currency Control</option>
                <option value="1">Cash Management</option>
                <option value="1">Information</option>
                <option value="1">Loans</option>
                <option value="1">Notification</option>
                <option value="1">Payments</option>
                <option value="1">Technical Support</option>
                <option value="1">Factoring:request/info</option>
                <option value="1">KYC</option>
                <option value="1">test Request certified copy of payment order</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1"></div>
            <div class="col-md-3 d-flex p-2 mt-1">From</div>
            <div class="col-md-3 d-flex p-2 mt-1">To</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1"> Sending date</div>
            <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>

        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Search for keyword in subject or body</div>
            <div class="input-group-sm col-md-8 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
        </div>

        <div class="row col-md-12 mb-2">
            <button type="button" class="btn btn-danger ml-3 mt-3 mb-2">Search ></button>
            <button type="button" class="btn btn-danger bg-white text-danger ml-3 mt-3 mb-2">Reset ></button>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12 mt-3">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Date</b></small></th>
            <th scope="col"><small><b>Subject</small></th>
            <th scope="col"><small><b>Topic</b></small></th>
            <th scope="col"><small><b>Attachment</b></small></th>
            <th scope="col"><small><b>Size(kb)</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <div class="shadow mt-3">
        <div class="row col-md-12 mb-2">
            <form class="mt-2 ml-3" action="#">
                <input class="text-danger border-danger bg-white" type="submit" value="Save attachments >"/>
            </form>
            <div class="d-flex p-2 mt-1 ml-3"><small>Build folder structure inside ZIP</small></div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2 mt-1 ml-3"><small>Add category and date prefix to file name</small></div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        </div>
    </div>

</div>
</#macro>