<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/search.ftl" as se>
<#import "../awesome/info.ftl" as info>
<#import "../awesome/save.ftl" as sav>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/sync.ftl" as sy>

<#macro for_fx_online>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-8 pl-5"><strong>ONLINE RATE FX ORDER</strong></h5>
        <div class="col-md-4 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Status</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small><strong>New</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Type</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>FX Order</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Document No.</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "3"/></div>
            <div class="col-md-1 d-flex p-2 ml-2 text-secondary"><small><strong>Date</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>SELL</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 ">
                <option value="acc1">Acc-1</option>
                <option value="acc2">Acc-2</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="col-md-1 d-flex p-2 ml-5 mr-2 text-secondary"><small><strong>Balance</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex justify-content-end p-2 fa"><@sy.sync/></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "8"/></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>BUY</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 ">
                <option value="acc1">Acc-1</option>
                <option value="acc2">Acc-2</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="col-md-1 d-flex p-2 ml-5 mr-2 text-secondary"><small><strong>Balance</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex justify-content-end p-2 fa"><@sy.sync/></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "8"/></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>Exchange Rate</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Online rate</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <button class="btn-sm ml-3">Request Rate</button>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small></small></div>
            <div class="col-md-8 d-flex p-2 text-danger"><small>Online exchange rates can be changed by the Bank during the day. We kindly recommend to request rates immediately before signing and sending the document to the Bank.</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>Order execution rules</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="rul" data-toggle="collapse" data-target="#rules" onclick="rules()"></div>
        </div>
        <div class="collapse row col-md-12 ml-1" id="rules">
            <div class="col-md-11 d-flex p-2"><small>"The Rules on execution by AO UniCredit Bank of non-cash transactions for purchase/sell of foreign currency according to applications of clients – legal entities and individual entrepreneurs" are known to us and we consider them to be obligatory for us.</small></div>
        </div>
    </div>
    <div class="mt-3 col-md-12">
        <div class="row">
            <div class="ml-4 pb-3 mt-1">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Save & sign ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & new ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & reuse ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="/foreign_payments_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Exit ></strong></small></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var fil1 = false;
    function rules(){
        if(!fil1){
            fil1 = true;
            document.getElementById("rul").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil1 = false;
            document.getElementById("rul").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
</script>
</#macro>