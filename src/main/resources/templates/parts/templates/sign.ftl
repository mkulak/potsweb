<#macro sign>
<div class="col-md-12 shadow">
    <h4 class="text-danger">SIGNATURE FOLDER</h4>
    <div class="col-md-12 shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Document type</div>
            <select class="custom-select-sm col-md-3 mt-2">
                <option value="1">All Types</option>
                <option value="2">Domestic Payments</option>
                <option value="3">Foreign Payments</option>
                <option value="2">Freeform Orders</option>
                <option value="3">Confirming Document Certificates</option>
                <option value="2">Currency Operations Certificates</option>
                <option value="3">New Agreement</option>
                <option value="2">Term Deposits</option>
                <option value="3">Packaged Orders</option>
            </select>
            <div class="col-md-3 d-flex justify-content-center p-2 mt-1">Status</div>
            <select class="custom-select-sm col-md-3 mt-2">
                <option value="1">All</option>
                <option value="2">To be signed</option>
                <option value="3">In sign process</option>
                <option value="2">Partly signed</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Company</div>
            <select class="custom-select-sm col-md-9 mt-2">
                <option value="1">All</option>
                <option value="2">Comp1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">From Account</div>
            <select class="custom-select-sm col-md-9 mt-2">
                <option value="1">All</option>
                <option value="2">Acc1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Date from/to</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="col-md-2 d-flex p-2 mt-1">Last days</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <select class="custom-select-sm col-md-2 mt-2">
                <option value="1">15</option>
                <option value="2">1</option>
                <option value="2">2</option>
                <option value="2">3</option>
                <option value="2">4</option>
                <option value="2">5</option>
                <option value="2">6</option>
                <option value="2">7</option>
                <option value="2">8</option>
                <option value="2">9</option>
                <option value="2">10</option>
                <option value="2">20</option>
                <option value="2">30</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Transactions</div>
            <input class="col-md-1 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="col-md-5 d-flex p-2 mt-1">Do not show orders signed by me</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1"></div>
            <input class="col-md-1 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="col-md-5 d-flex p-2 mt-1">Show orders I can sign</div>
        </div>
        <div class="mt-1 col-md-12">
            <a class="btn btn-danger mb-2" data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="filter">
                More search criteria
            </a>
            <div class="collapse mt-1" id="filter">
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Receiver name</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-3 d-flex p-2 mt-1">INN</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Account / IBAN</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-3 d-flex p-2 mt-1">SWIFT / BIC</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Amount from / to</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-2 d-flex p-2 mt-1">Currency</div>
                    <div class="input-group-sm col-md-1 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Payment details</div>
                    <div class="input-group-sm col-md-6 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-2 d-flex p-2 mt-1">Order id</div>
                    <div class="input-group-sm col-md-1 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <form class="mt-2 ml-3 mb-2" action="#">
                <input class="text-danger border-danger" type="submit" value="Select >"/>
            </form>
            <form class="mt-2 ml-3 mb-2" action="#">
                <input class="text-danger border-danger" type="submit" value="Clear >"/>
            </form>
            <input class="col-md-1 mt-3 mb-2 ml-2" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="col-md-3 d-flex p-2 mt-1">result max.10000. lines</div>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12 mt-3">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Status<br><br>N of sign</b></small></th>
            <th scope="col"><small><b>Document type</b></small></th>
            <th scope="col"><small><b>Account<br><br>Order details</b></small></th>
            <th scope="col"><small><b>Date from/to</b></small></th>
            <th scope="col"><small><b>No</b></small></th>
            <th scope="col"><small><b>Amount from/to</b></small></th>
            <th scope="col"><small><b>Curr</b></small></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>

</#macro>