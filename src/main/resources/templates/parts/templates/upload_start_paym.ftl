<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/info.ftl" as info>

<#macro upload_start_paym>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-7 pl-5"><strong>START UPLOAD</strong></h5>
        <div class="col-md-5 d-flex justify-content-end p-2 mt-2 fa pr-4">
            <@fa.fav/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Client</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 mt-2">
                <option value="all">Default</option>
                <option value="cli1">Cli-1</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option selected>Select Payment Type</option>
                <option value="dom">Domestic Payment</option>
                <option value="for">Foreign Payment</option>
                <option value="cc">Currency Control</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Description</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Filename</strong></small></div>
            <input type="file" class="form-control-sm col-md-8 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>File encoding</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option selected>Select file encoding</option>
                <option value="cp">Windows(CP1251)</option>
                <option value="utf">Universal(UTF-8)</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>File Structure</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 mb-2">
                <option selected>Select Structure Parameter</option>
                <option value="c">1C</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "12"/></div>
        </div>
    </div>
    <div class="row col-md-12 ml-1 mt-2">
        <div class="mt-1 ml-2 pb-3">
            <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Start ></strong></small></a>
        </div>
    </div>
</div>

</#macro>