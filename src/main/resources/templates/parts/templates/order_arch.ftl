<#macro ord_arch>
<div class="col-md-12 shadow">
    <h4 class="text-danger">ORDER ARCHIVE</h4>
    <div class="col-md-12 shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
            <select class="custom-select-sm col-md-3 mt-2">
                <option value="1">All</option>
                <option value="2">Domestic Payments</option>
                <option value="3">Foreign Payments</option>
                <option value="2">Freeform Orders</option>
                <option value="3">Confirming Document Certificates</option>
                <option value="2">Currency Operations Certificates</option>
                <option value="3">New Agreement</option>
                <option value="2">Term Deposits</option>
                <option value="3">Employee Management Orders</option>
                <option value="3">Salary Sheets</option>
            </select>
            <div class="col-md-3 d-flex justify-content-center p-2 mt-1">Status</div>
            <select class="custom-select-sm col-md-3 mt-2">
                <option value="1">All</option>
                <option value="2">All accepted</option>
                <option value="3">Canceled</option>
                <option value="2">Signed</option>
                <option value="3">Booked</option>
                <option value="2">Rejected</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Organization</div>
            <select class="custom-select-sm col-md-9 mt-2">
                <option value="1">All</option>
                <option value="2">Comp1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Account</div>
            <select class="custom-select-sm col-md-9 mt-2">
                <option value="1">All</option>
                <option value="2">Acc1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Order date from/to</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2 mt-2"/>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2 mt-2"/>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1"></div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <select class="custom-select-sm col-md-2 mt-2">
                <option value="1">15</option>
                <option value="2">1</option>
                <option value="2">2</option>
                <option value="2">3</option>
                <option value="2">4</option>
                <option value="2">5</option>
                <option value="2">6</option>
                <option value="2">7</option>
                <option value="2">8</option>
                <option value="2">9</option>
                <option value="2">10</option>
                <option value="2">20</option>
                <option value="2">30</option>
            </select>
            <div class="col-md-2 d-flex p-2 mt-1">Last days</div>
        </div>
        <div class="mt-1 col-md-12">
            <a class="btn btn-danger mb-2" data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="filter">
                More search criteria
            </a>
            <div class="collapse mt-1" id="filter">
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Beneficiary Name</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-3 d-flex p-2 mt-1">Benef. INN</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Partner Account/IBAN</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-3 d-flex p-2 mt-1">SWIFT / BIC</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Amount from / to</div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="input-group-sm col-md-3 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-2 d-flex p-2 mt-1">Currency</div>
                    <div class="input-group-sm col-md-1 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Transaction Details</div>
                    <div class="input-group-sm col-md-6 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                    <div class="col-md-2 d-flex p-2 mt-1">Document No.</div>
                    <div class="input-group-sm col-md-1 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <form class="mt-2 ml-3 mb-2" action="#">
                <input class="text-danger border-danger" type="submit" value="Select >"/>
            </form>
            <form class="mt-2 ml-3 mb-2" action="#">
                <input class="text-danger border-danger" type="submit" value="Clear >"/>
            </form>
            <input class="col-md-1 mt-3 mb-2 ml-2" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="col-md-3 d-flex p-2 mt-1">result max.10000. lines</div>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12 mt-3">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Status</b></small></th>
            <th scope="col"><small><b>Order type</b></small></th>
            <th scope="col"><small><b>Order details</b></small></th>
            <th scope="col"><small><b>Date</b></small></th>
            <th scope="col"><small><b>No</b></small></th>
            <th scope="col"><small><b>Amount</b></small></th>
            <th scope="col"><small><b>Curr</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>
</#macro>