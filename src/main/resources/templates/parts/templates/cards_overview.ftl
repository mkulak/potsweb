<#import "../cards_overview/credit_card_table.ftl" as cr>
<#import "../cards_overview/debit_card_table.ftl" as de>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro card_over>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5"><strong>CARDS OVERVIEW</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1 mt-2 pt-2">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Card Number</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Card Holder</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Full Name</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
        </div>
        <div class="row col-md-7">
            <div class="mt-3 ml-4 pb-3">
                <a href="" class="btn btn-danger" onclick="update()" style="border-radius: 8px"><small><strong>Search ></strong></small></a>
            </div>
            <div class="mt-3 ml-4 pb-3">
                <a href="/cards_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Reset ></strong></small></a>
            </div>
        </div>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="col-md-3 d-flex p-2 text-secondary mt-3"><small><strong>Debit Cards Overview</strong></small></div>
        <div class="fas fa-plus-circle text-secondary fa-sm mt-4 col-md-1" id="deb" data-toggle="collapse" data-target="#debit_cards" onclick="debit()"></div>
    </div>
    <div id="debit_cards" class="collapse col-md-12">
        <@de.debit_card/>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Credit Cards Overview</strong></small></div>
        <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="cre" data-toggle="collapse" data-target="#credit_cards" onclick="credit()"></div>
    </div>
    <div id="credit_cards" class="collapse col-md-12">
        <@cr.credit_card/>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>History</strong></small></div>
        <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="his" data-toggle="collapse" data-target="#history" onclick="hist()"></div>
    </div>
    <div class="row col-md-12 ml-1 collapse pb-2" id="history">
        <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transactions of card no.</strong></small></div>
        <select class="custom-select-sm col-md-7 bg-white d-flex p-2" id="days">
            <option value="acc1">Acc-1</option>
            <option value="acc2">Acc-2</option>
        </select>
        <div class="col-md-2 d-flex p-2"><small>for the last 15 days</small></div>
    </div>
</div>
<script type="text/javascript">
    var deb = false;
    function debit(){
        if(!deb){
            deb = true;
            document.getElementById("deb").className = "fas fa-minus-circle text-secondary fa-sm mt-4 col-md-1";
        } else {
            deb = false;
            document.getElementById("deb").className = "fas fa-plus-circle text-secondary fa-sm mt-4 col-md-1";
        }
    }
    var cre = false;
    function credit(){
        if(!cre){
            cre = true;
            document.getElementById("cre").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            cre = false;
            document.getElementById("cre").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
    var his = false;
    function hist(){
        if(!his){
            his = true;
            document.getElementById("his").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            his = false;
            document.getElementById("his").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
</#macro>