<#import "../for_paym_create/for_paym_notif.ftl" as not>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/search.ftl" as se>
<#import "../awesome/info.ftl" as info>
<#import "../awesome/save.ftl" as sav>
<#import "../awesome/house.ftl" as ho>
<#import "../awesome/help.ftl" as he>

<#macro for_buy_cur>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-8 pl-5"><strong>BUY</strong></h5>
        <div class="col-md-4 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@he.help/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Templates</strong></small></div>
            <select class="custom-select-sm col-md-8 mt-2 bg-white d-flex p-2 ">
                <option selected>Select a Template</option>
                <option value="temp1">Template-1</option>
            </select>
            <div class="d-flex justify-content-end p-2 mt-2 fa">
            <@se.search "1"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Status</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small><strong>New</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Type</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Currency Buy</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 ">
                <option value="acc1">Acc-1</option>
                <option value="acc2">Acc-2</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer Name</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer INN</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>OKPO</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Sender Official</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <input class="d-flex p-2 ml-1 mt-2" type="checkbox"/>
            <div class="d-flex p-2"><small>Set as default</small></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "5"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Phone Official</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <input class="d-flex p-2 ml-1 mt-2" type="checkbox"/>
            <div class="d-flex p-2"><small>Set as default</small></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "6"/></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Payee Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 mt-2">
                <option selected>Select an Account</option>
                <option value="acc1">Acc-1</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Document No.</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "3"/></div>
            <div class="col-md-1 d-flex p-2 ml-2 text-secondary"><small><strong>Date</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Deal</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" disabled>
                <option value="rate">at Banks exchange rate</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Exchanged Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Maximum Rate</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Valid Till Date</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "7"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>Order execution rules</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="rul" data-toggle="collapse" data-target="#rules" onclick="rules()"></div>
        </div>
        <div class="collapse row col-md-12 ml-1" id="rules">
            <div class="col-md-11 d-flex p-2"><small>"The Rules on execution by AO UniCredit Bank of non-cash transactions for purchase/sell of foreign currency according to applications of clients – legal entities and individual entrepreneurs" are known to us and we consider them to be obligatory for us.</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>One Time Notification</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="not" data-toggle="collapse" data-target="#notif" onclick="notif()"></div>
        </div>
        <div class="collapse row col-md-12 ml-1" id="notif">
            <@not.for_paym_not/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-danger"><small><strong>Save Document Template</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-3 ml-2" id="save" data-toggle="collapse" data-target="#template" onclick="save()"></div>
        </div>
        <div class="collapse mt-1" id="template">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
                <@sav.print "2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio3" id="blankRadio3" checked>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio4" id="blankRadio4">
                <div class="d-flex justify-content-end p-2 fa">
                    <@info.fav "4"/>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 col-md-12">
        <div class="row">
            <div class="ml-4 pb-3 mt-1">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Save & sign ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & new ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & reuse ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="/foreign_payments_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Exit ></strong></small></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var fil1 = false;
    function rules(){
        if(!fil1){
            fil1 = true;
            document.getElementById("rul").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil1 = false;
            document.getElementById("rul").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
    var fil2 = false;
    function notif(){
        if(!fil2){
            fil2 = true;
            document.getElementById("not").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil2 = false;
            document.getElementById("not").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
    var fil3 = false;
    function save(){
        if(!fil3){
            fil3 = true;
            document.getElementById("save").className = "fas fa-minus-circle text-secondary fa-sm mt-3 ml-2";
        } else {
            fil3 = false;
            document.getElementById("save").className = "fas fa-plus-circle text-secondary fa-sm mt-3 ml-2";
        }
    }
</script>


</#macro>