<#import "../for_paym_create/for_paym_filter.ftl" as fi>
<#import "../for_paym_create/for_paym_notif.ftl" as not>
<#import "../for_paym_create/for_cur_control_table.ftl" as cur>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/search.ftl" as se>
<#import "../awesome/info.ftl" as info>
<#import "../awesome/save.ftl" as sav>
<#import "../awesome/house.ftl" as ho>

<#macro for_paym_transit>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-8 pl-5"><strong>FOREIGN PAYMENT WITH TRANSIT ACCOUNT</strong></h5>
        <div class="col-md-4 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Templates</strong></small></div>
            <select class="custom-select-sm col-md-8 mt-2 bg-white d-flex p-2 ">
                <option selected>Select a Template</option>
                <option value="temp1">Template-1</option>
            </select>
            <div class="d-flex justify-content-end p-2 mt-2 fa">
                <@se.search "1"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Status</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small><strong>New</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Type</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Foreign Payment from transit account</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 ">
                <option value="acc1">Acc-1</option>
                <option value="acc2">Acc-2</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer Name</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer INN</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Value</small></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Beneficiary(Partner)</strong></small></div>
            <select class="custom-select-sm col-md-8 mt-2 bg-white d-flex p-2 ">
                <option selected>Select a Template</option>
                <option value="temp1">Template-1</option>
            </select>
            <div class="d-flex justify-content-end p-2 mt-2 fa"><@se.search "2"/></div>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Country Code</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "6"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary City</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Address</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Account</strong></small></div>
            <input type="text" class="form-control-sm col-md-7 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <input class="d-flex p-2 ml-1 mt-2" type="checkbox"/>
            <div class="d-flex p-2"><small>IBAN</small></div>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Bank Name</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="mt-1"><@ho.home "1"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>BIC /SWIFT</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex justify-content-end p-2 fa ml-3"><@se.search "5"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank Country Code</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "7"/></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank City</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank Address</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Branch Name and other requisites</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Interm. Bank Name</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="interm" data-toggle="collapse" data-target="#filter" onclick="interm()"></div>
        </div>
        <div class="collapse" id="filter">
            <@fi.for_paym_create/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Document No.</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "3"/></div>
            <div class="col-md-1 d-flex p-2 ml-2"><small>Date</small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="col-md-1 d-flex p-2 ml-3"><small>Currency</small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2 ml-4"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "8"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"></div>
            <div class="col-md-1 d-flex p-2"><small>FullPay</small></div>
            <input class="col-md-1 d-flex p-2 mt-2" type="checkbox"/>
            <div class="col-md-2 d-flex justify-content-end p-2 mr-3 ml-5"><small>Urgency</small></div>
            <select class="custom-select-sm col-md-3 bg-white d-flex p-2 ">
                <option value="norm">Normal</option>
                <option value="urg">Urgency</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Details</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Additional info</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control" disabled></textarea>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Code instruction</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Charges type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option value="our">OUR - all fees covered by payer</option>
                <option value="sha">SHA - domestic fees to payer, foreign fees to payee</option>
                <option value="ben">BEN - all fees covered by payee</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Charges type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option value="acc1">Acc1</option>
                <option value="acc2">Acc2</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>One Time Notification</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="not" data-toggle="collapse" data-target="#notif" onclick="notif()"></div>
        </div>
        <div class="collapse mt-1" id="notif">
            <@not.for_paym_not/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-5 d-flex p-2 mt-2"><small><strong>Currency Control Information</strong></small></div>
        </div>
        <div class="col-md-12">
            <@cur.cur_control/>
        </div>
        <div class="row">
            <div class="mt-1 ml-4 pb-3">
                <a href="#" class="btn btn-outline-danger ml-2 mt-2" style="border-radius: 8px"><small><strong>Add line ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger mt-2" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>Save Partner</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="part" data-toggle="collapse" data-target="#partner" onclick="partner()"></div>
        </div>
        <div class="collapse mt-1" id="partner">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
                <@sav.print "1"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner Group Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio1" id="blankRadio1" checked>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio2" id="blankRadio2">
                <div class="d-flex justify-content-end p-2 fa">
                    <@info.fav "4"/>
                </div>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>Save Document Template</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="temp" data-toggle="collapse" data-target="#template" onclick="template()"></div>
        </div>
        <div class="collapse mt-1" id="template">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
                <@sav.print "2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio3" id="blankRadio3" checked>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio4" id="blankRadio4">
                <div class="d-flex justify-content-end p-2 fa">
                    <@info.fav "4"/>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-3 col-md-12">
        <div class="row">
            <div class="ml-4 pb-3 mt-1">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Save & sign ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & new ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & reuse ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="/foreign_payments_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Exit ></strong></small></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var fil4 = false;
    function interm(){
        if(!fil4){
            fil4 = true;
            document.getElementById("interm").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil4 = false;
            document.getElementById("interm").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
    var fil5 = false;
    function notif(){
        if(!fil5){
            fil5 = true;
            document.getElementById("not").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil5 = false;
            document.getElementById("not").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
    var fil6 = false;
    function partner(){
        if(!fil6){
            fil6 = true;
            document.getElementById("part").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil6 = false;
            document.getElementById("part").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
    var fil7 = false;
    function template(){
        if(!fil7){
            fil7 = true;
            document.getElementById("temp").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil7 = false;
            document.getElementById("temp").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
</script>


</#macro>