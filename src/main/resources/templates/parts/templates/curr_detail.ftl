<#macro curr_detail>
<div class="col-md-12 shadow">
    <h3 class="text-danger">DETAIL FOR CURRENCY CONTROL</h3>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order status</div>
        <div class="col-md-6 d-flex p-2 mt-1">New</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
        <div class="col-md-6 d-flex p-2 mt-1">New detail for Currency Control</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Client</div>
        <select class="custom-select-sm col-md-8 mt-2">
            <option selected>Select a client</option>
            <option value="1">Cli1</option>
            <option value="2">Cli2</option>
            <option value="3">Cli3</option>
        </select>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Account</div>
        <select class="custom-select-sm col-md-8 mt-2">
            <option selected>Select an account</option>
            <option value="1">Acc1</option>
            <option value="2">Acc2</option>
            <option value="3">Acc3</option>
        </select>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">COC Doc No.</div>
        <div class="input-group-sm col-md-2 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
        <div class="col-md-3 d-flex justify-content-end p-2 mt-1">COC Doc Date</div>
        <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Country Code</div>
        <div class="input-group-sm col-md-2 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12">
        <thead class="table-primary">
        <tr>
            <th scope="col"><small><b>Line</b></small></th>
            <th scope="col"><small><b>UC № or № Contract/date of Contract</b></small></th>
            <th scope="col"><small><b>Operation Date</b></small></th>
            <th scope="col"><small><b>VO Code</b></small></th>
            <th scope="col"><small><b>Amount</b></small></th>
            <th scope="col"><small><b>Curr</b></small></th>
            <th scope="col"><small><b>No docs required</b></small></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <form class="mt-2 ml-3" action="#">
        <input class="text-danger border-danger" type="submit" value="Add new line >"/>
    </form>

    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Sign >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Reuse >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Print CBR >"/>
        </form>
        <form class="mt-2 ml-3" action="/currency_control_overview">
            <input class="text-danger border-danger" type="submit" value="Exit >"/>
        </form>
    </div>

</div>

</#macro>