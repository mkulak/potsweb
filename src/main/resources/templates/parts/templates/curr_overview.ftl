<#macro curr_overview>
<div class="col-md-12 shadow">
    <h3 class="text-danger">CONTRACT & CURRENCY OPERATIONS OVERVIEW</h3>
    <div class="col-md-12 shadow p-1 mb-2 rounded-lg">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Client</div>
            <select class="custom-select-sm col-md-8 mt-2">
                <option value="1">Cli1</option>
                <option value="2">Cli2</option>
                <option value="3">Cli3</option>
            </select>
            <div class="d-flex p-2 mt-1 text-danger">*</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Status</div>
            <select class="custom-select-sm col-md-8 mt-2">
                <option selected>All</option>
                <option value="1">To Sign</option>
                <option value="2">In Progress</option>
                <option value="3">Executed</option>
                <option value="3">Return without execution</option>
                <option value="3">Partially signed</option>
                <option value="3">Signature in process</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Document types</div>
            <div class="d-flex p-2 mt-1">DP/UC</div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2 mt-1">COC</div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2 mt-1">CDC</div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2 mt-1">CD</div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2 mt-1">Contract 138-I</div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Date from/to</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="col-md-2 d-flex p-2 mt-1">Last days</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <select class="custom-select-sm col-md-2 mt-2">
                <option selected>15</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="3">4</option>
                <option value="3">5</option>
                <option value="3">6</option>
                <option value="3">7</option>
                <option value="3">8</option>
                <option value="3">9</option>
                <option value="3">10</option>
                <option value="3">20</option>
                <option value="3">30</option>
            </select>
        </div>
        <div class="mt-1 col-md-12">
            <a class="btn btn-danger" data-toggle="collapse" href="#more" role="button" aria-expanded="false" aria-controls="filter">
                More search criteria
            </a>
            <div class="collapse mt-1" id="more">
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2 mt-1">Country</div>
                    <select class="custom-select-sm col-md-9 mt-2">
                        <option selected>All</option>
                        <option value="1">Country1</option>
                    </select>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2">№ DP/UC</div>
                    <div class="input-group-sm col-md-9 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
                <div class="row col-md-12">
                    <div class="col-md-3 d-flex p-2">Number of Contract</div>
                    <div class="input-group-sm col-md-9 d-flex p-2">
                        <input type="text" class="form-control"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <button type="button" class="btn btn-danger ml-3 mt-3">Search ></button>
            <button type="button" class="btn btn-danger ml-2 mt-3">Reset ></button>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Document type<br>Document number</b></small></th>
            <th scope="col"><small><b>Status<br>Date from - until</b></small></th>
            <th scope="col"><small><b>Amount<br>Currency</b></small></th>
            <th scope="col"><small><b>Document information</b></small></th>
            <th scope="col"><small><b>Actions</b></small></th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div class="row">
        <form class="col-md-2 mt-2" action="/currency_control_cdc">
            <input class="text-danger border-danger" type="submit" value="Create CDC >"/>
        </form>
        <form class="col-md-2 mt-2" action="/currency_control_detail">
            <input class="text-danger border-danger" type="submit" value="Create COC>"/>
        </form>
    </div>
</div>


</#macro>