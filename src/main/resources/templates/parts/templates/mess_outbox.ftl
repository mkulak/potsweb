<#macro mess_outbox>
<div class="col-md-12 shadow">
    <h4 class="text-danger"><strong>OUTBOX</strong></h4>
    <div class="shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Status</div>
            <select class="custom-select-sm col-md-8 d-flex p-2 mt-1">
                <option value="1">All</option>
                <option value="2">To be signed</option>
                <option value="3">P.signed</option>
                <option value="1">Booked</option>
                <option value="1">Canceled</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Clients</div>
            <select class="custom-select-sm col-md-8 d-flex p-2 mt-1">
                <option value="1">All</option>
                <option value="2">Cli1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Category</div>
            <select class="custom-select-sm col-md-8 d-flex p-2 mt-1">
                <option selected>Select a category</option>
                <option value="1">001.Loan. Notice of Disbursement</option>
                <option value="2">002.Loan. Notice of Prepayment</option>
                <option value="3">003.Loan. Cancellation Notice of Disbursement\Notice of Prepayment</option>
                <option value="1">Balance Accruals</option>
                <option value="2">Cancel Document</option>
                <option value="3">Cards Requests</option>
                <option value="1">Collection:change/others</option>
                <option value="2">Conversion:confirmation of conditions</option>
                <option value="3">Corporate cards:request/info</option>
                <option value="1">Credit:request to curator</option>
                <option value="2">Currency control document(except VO inquiry)</option>
                <option value="3">Deposit:request/info</option>
                <option value="3">Factoring:request/info</option>
                <option value="3">Guarantees & standby LC:issuance/others</option>
                <option value="3">Letters of credit:opening/others</option>
                <option value="3">Order for depository</option>
                <option value="3">Other type</option>
                <option value="3">Overdraft:confirmation of conditions</option>
                <option value="3">Payments:change detail</option>
                <option value="3">Payments:urgent</option>
                <option value="3">Payroll file</option>
                <option value="3">Request for bank</option>
                <option value="3">Request of certified copy payment order</option>
                <option value="3">Request related to Business.Online contract</option>
                <option value="3">Request to support</option>
                <option value="3">Test Category</option>
                <option value="3">Transit account:other transfers</option>
                <option value="3">XT .4K Money Market Line</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Description</div>
            <div class="input-group-sm col-md-8 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Date from/to</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="col-md-2 d-flex p-2 mt-1">Last days</div>
            <div class="form-check d-flex p-2 mt-1">
                <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
            </div>
            <select class="custom-select-sm col-md-2 mt-2">
                <option value="1">15</option>
                <option value="2">1</option>
                <option value="2">2</option>
                <option value="2">3</option>
                <option value="2">4</option>
                <option value="2">5</option>
                <option value="2">6</option>
                <option value="2">7</option>
                <option value="2">8</option>
                <option value="2">9</option>
                <option value="2">10</option>
                <option value="2">20</option>
                <option value="2">30</option>
            </select>
        </div>
        <div class="row col-md-12 mb-2">
            <button type="button" class="btn btn-danger ml-3 mt-3 mb-2">Search ></button>
            <button type="button" class="btn btn-danger bg-white text-danger ml-3 mt-3 mb-2">Reset ></button>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12 mt-3">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Status</b></small></th>
            <th scope="col"><small><b>Date<br>No</b></small></th>
            <th scope="col"><small><b>Category</b></small></th>
            <th scope="col"><small><b>Description<br>Client</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <div class="row col-md-12 mb-2">
        <form class="mt-2 ml-3" action="/message_new_free_form_document">
            <input class="text-danger border-danger bg-white" type="submit" value="Create >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger bg-white" type="submit" value="Delete >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger bg-white" type="submit" value="Reuse >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger bg-white" type="submit" value="Print >"/>
        </form>
    </div>

</div>
</#macro>