<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/help.ftl" as he>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/info.ftl" as info>
<#import "../package_overview/import_upload_paym_table.ftl" as tab>

<#macro import_upload_paym>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-7 pl-5"><strong>PACKAGE OVERVIEW</strong></h5>
        <div class="col-md-5 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@he.help/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Status</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 mt-2">
                <option value="all">All</option>
                <option value="prep">Prepared</option>
                <option value="tobe_sign">To be signed</option>
                <option value="p_sign">P.signed</option>
                <option value="process">Sign in process</option>
                <option value="upload">Uploading</option>
                <option value="import">Importing</option>
                <option value="err">Error</option>
                <option value="err_ord">Error(Order)</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option value="all">All</option>
                <option value="dom">Domestic Payment</option>
                <option value="for">Foreign Payment</option>
                <option value="cc">Currency Control</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Timestamp from / to</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2 ml-3"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>More search criteria</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="cri" data-toggle="collapse" data-target="#filter" onclick="cri()"></div>
        </div>
        <div class="collapse" id="filter">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Checksum from / to</strong></small></div>
                <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
                <input type="text" class="form-control-sm col-md-3 d-flex p-2 ml-3"/>
                <div class="d-flex justify-content-end p-2 fa"><@info.fav "10"/></div>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Package details</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex justify-content-end p-2 fa"><@info.fav "11"/></div>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Select ></strong></small></a>
            </div>
            <div class="mt-1 ml-4 pb-3">
                <a href="/import_upload_payments" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Clear ></strong></small></a>
            </div>
            <input class="col-md-1 d-flex p-2 mt-3" type="checkbox" checked/>
            <div class="col-md-7 d-flex p-2 mt-2"><small>Auto refresh (will be deactivated, if everything finished)</small></div>
        </div>
    </div>
    <div class="d-flex p-2 mt-3">
        <@tab.upload_paym_table/>
    </div>
    <div class="row col-md-12 ml-1 mt-1">
        <div class="mt-1 ml-2 pb-3">
            <a href="/start_import_payments" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Import ></strong></small></a>
        </div>
        <div class="mt-1 ml-4 pb-3">
            <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Delete ></strong></small></a>
        </div>
        <div class="mt-1 ml-2 pb-3">
            <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>To signature folder ></strong></small></a>
        </div>
        <div class="mt-1 ml-4 pb-3">
            <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    var more6 = false;
    function cri(){
        if(!more6){
            more6 = true;
            document.getElementById("cri").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            more6 = false;
            document.getElementById("cri").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>

</#macro>