<#import "../awesome/favorites.ftl" as fa>

<#macro benef_li_partn_import>

<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-7 pl-5"><strong>IMPORT PARTNERS</strong></h5>
        <div class="col-md-5 d-flex justify-content-end p-2 mt-2 fa pr-4">
            <@fa.fav/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Filename</strong></small></div>
            <input type="file" class="form-control-sm col-md-8 d-flex p-2 mt-1"/>
            <div class="d-flex p-2 text-danger mt-2"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option selected>Select a Type</option>
                <option value="dom">Domestic payment Partner</option>
                <option value="for">Foreign payment Partner</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Format</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2">
                <option value="csv">CSV</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" checked>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public for</strong></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1">
            <select class="custom-select-sm col-md-7 bg-white d-flex p-2 ml-4">
                <option value="cli1">Cli-1</option>
                <option value="cli2">Cli-2</option>
            </select>
        </div>
    </div>
    <div class="row col-md-12 ml-1 mt-2">
        <div class="mt-1 ml-2 pb-3">
            <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Start ></strong></small></a>
        </div>
    </div>
</div>

</#macro>