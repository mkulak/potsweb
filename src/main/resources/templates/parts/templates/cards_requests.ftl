<#macro card_req>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5"><strong>CARDS REQUESTS</strong></h5>
    </div>
    <div class="row col-md-12 ml-1 mt-2 pt-2">
        <div class="col-md-3 d-flex p-2 text-secondary"><small>Portlet: Cards requests</small></div>
    </div>
</div>
</#macro>