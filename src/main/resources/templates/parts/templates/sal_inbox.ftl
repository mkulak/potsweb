<#macro sal_inbox>
<div class="col-md-12 shadow">
    <h4 class="text-danger">SALARY INBOX KU</h4>
    <div class="col-md-12 shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Topic</div>
            <select class="custom-select-sm col-md-8 mt-2">
                <option selected>All</option>
                <option value="1">Payroll Statement</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1"></div>
            <div class="col-md-3 d-flex p-2 mt-1">From</div>
            <div class="col-md-3 d-flex p-2 mt-1">To</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Sending date</div>
            <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Search for keyword in subject or body</div>
            <div class="input-group-sm col-md-8 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-4 d-flex p-2 mt-1">Show deleted messages</div>
            <input class="col-md-1 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="col-md-4 d-flex p-2 mt-1">Show only unread messages</div>
            <input class="mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        </div>
        <div class="row col-md-12">
            <form class="mt-2 ml-3" action="#">
                <input class="text-danger border-danger" type="submit" value="Search >"/>
            </form>
            <form class="col-md-2 mt-2 ml-3" action="#">
                <input class="text-danger border-danger" type="submit" value="Reset >"/>
            </form>
            <input class="col-md-1 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="col-md-4 d-flex p-2 mt-1">result max. . lines</div>
        </div>
    </div>
    <table class="table-sm table-striped col-md-12 mt-2">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Date</b></small></th>
            <th scope="col"><small><b>Subject</b></small></th>
            <th scope="col"><small><b>Topic</b></small></th>
            <th scope="col"><small><b>Attachment</b></small></th>
            <th scope="col"><small><b>Size(kb)</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div class="col-md-12 shadow mt-2 mb-2">
        <div class="row col-md-12">
            <form class="mt-2 ml-3" action="#">
                <input class="text-danger border-danger" type="submit" value="Save Attachments >"/>
            </form>
            <div class="d-flex p-2 mt-1 ml-3"><small>Build folder structure inside ZIP</small></div>
            <input class="mt-3 ml-1" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2 mt-1 ml-3"><small>Add category and date prefix to file name</small></div>
            <input class="mt-3 ml-1" type="checkbox" aria-label="Checkbox for following text input"/>
        </div>
    </div>
</div>
</#macro>