<#macro mess_free_form_doc>
<div class="col-md-12 shadow">
    <h4 class="text-danger"><strong>FREE-FORMAT DOCUMENT</strong></h4>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Number</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="text-danger d-flex p-2 mt-1">*</div>
        <div class="col-md-1 d-flex p-2 mt-1">Date</div>
        <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        <div class="text-danger d-flex p-2 mt-1">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Client</div>
        <select class="custom-select-sm col-md-8 d-flex p-2">
            <option selected>Select a client</option>
            <option value="1">Cli1</option>
        </select>
        <div class="text-danger d-flex p-2">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Category</div>
        <select class="custom-select-sm col-md-8 d-flex p-2">
            <option selected>Select a category</option>
            <option value="1">001.Loan. Notice of Disbursement</option>
            <option value="2">002.Loan. Notice of Prepayment</option>
            <option value="3">003.Loan. Cancellation Notice of Disbursement\Notice of Prepayment</option>
            <option value="1">Balance Accruals</option>
            <option value="2">Cancel Document</option>
            <option value="3">Cards Requests</option>
            <option value="1">Collection:change/others</option>
            <option value="2">Conversion:confirmation of conditions</option>
            <option value="3">Corporate cards:request/info</option>
            <option value="1">Credit:request to curator</option>
            <option value="2">Currency control document(except VO inquiry)</option>
            <option value="3">Deposit:request/info</option>
            <option value="3">Factoring:request/info</option>
            <option value="3">Guarantees & standby LC:issuance/others</option>
            <option value="3">Letters of credit:opening/others</option>
            <option value="3">Order for depository</option>
            <option value="3">Other type</option>
            <option value="3">Overdraft:confirmation of conditions</option>
            <option value="3">Payments:change detail</option>
            <option value="3">Payments:urgent</option>
            <option value="3">Payroll file</option>
            <option value="3">Request for bank</option>
            <option value="3">Request of certified copy payment order</option>
            <option value="3">Request related to Business.Online contract</option>
            <option value="3">Request to support</option>
            <option value="3">Test Category</option>
            <option value="3">Transit account:other transfers</option>
            <option value="3">XT .4K Money Market Line</option>
        </select>
        <div class="text-danger d-flex p-2">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Description</div>
        <div class="input-group-sm col-md-8 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Message</div>
        <textarea class="custom-select-sm col-md-8 mt-1 form-control"></textarea>
    </div>
    <form class="mt-2 ml-3 mb-2" action="#">
        <input class="text-danger border-danger" type="submit" value="Attachments >"/>
    </form>
    <a class="btn btn-danger mb-2" data-toggle="collapse" href="#history" role="button" aria-expanded="false" aria-controls="filter">
        Orderhistory
    </a>
    <div class="collapse mt-1" id="history">


    </div>
    <div class="row col-md-12 mb-2">
        <button type="button" class="btn btn-danger ml-3 mt-3">Save & Sign ></button>
        <button type="button" class="btn btn-danger ml-3 mt-3">Save & Reuse ></button>
        <button type="button" class="btn btn-danger ml-3 mt-3">Save ></button>
    </div>
</div>
</#macro>