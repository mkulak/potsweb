<#macro curr_new_agree>
<div class="col-md-12 shadow">
    <h3 class="text-danger">NEW AGREEMENT</h3>
    <small><strong>Attention! This form must use only to open a New agreement (agreement, information about export contract)!</strong></small>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order status</div>
        <div class="col-md-3 d-flex p-2 mt-1">New</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
        <div class="col-md-3 d-flex p-2 mt-1">New agreement</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Document number</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
        <div class="col-md-3 d-flex p-2 mt-1">Document date</div>
        <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">UC №</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="col-md-3 d-flex p-2 mt-1 ml-4">Date of UC №</div>
        <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">(xxxxxxxx/xxxx/xxxx/x/x)</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Sender official</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        <div class="col-md-3 d-flex p-2 mt-1">Set as default</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Phone official</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        <div class="col-md-3 d-flex p-2 mt-1">Set as default</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Branch office</div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Select branch office</option>
            <option value="1">A11</option>
        </select>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Type of contract</div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Select type of contract</option>
            <option value="1">Export of goods</option>
            <option value="1">Export of works/services</option>
            <option value="1">Export of goods & export of works/services</option>
            <option value="1">other</option>
        </select>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Need registration</div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Choose value</option>
            <option value="1">Yes</option>
            <option value="1">No</option>
        </select>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1"><strong>1. Client</strong></div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Select a client</option>
            <option value="1">Cli1</option>
        </select>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">1.1. Name</div>
        <div class="input-group-sm col-md-8 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">1.2. Address:</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">RF Subject (Region)</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
        <div class="col-md-2 d-flex p-2 mt-1">Area</div>
        <div class="input-group-sm col-md-3 d-flex p-2 ml-3">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">City</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 ml-4">Population Center</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Street</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="col-md-2 d-flex p-2 mt-1 ml-4">House</div>
        <div class="input-group-sm col-md-3 d-flex p-2 ml-3">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Building(Housing)</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="col-md-2 d-flex p-2 mt-1 ml-4">Office(Appartment)</div>
        <div class="input-group-sm col-md-3 d-flex p-2 ml-3">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">1.3. OGRN</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">1.4. State regist. date</div>
        <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">INN</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
        <div class="col-md-2 d-flex p-2 mt-1">KPP</div>
        <div class="input-group-sm col-md-3 d-flex p-2 ml-3">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2 mt-1"><strong>2. Information about nonresident</strong></div>
    </div>
    <table class="table-sm table-striped col-md-12">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>Nonresident</b></small></th>
            <th scope="col"><small><b>Country code</b></small></th>
            <th scope="col"><small><b>Country</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div class="collapse" id="more">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Nonresident</div>
            <select class="custom-select-sm col-md-8 mt-2">
                <option selected>Select a partner</option>
                <option value="1">Partn1</option>
            </select>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2">Name</div>
            <textarea class="custom-select-sm col-md-8 mt-1 form-control"></textarea>
            <div class="d-flex p-2 mt-2 text-danger">*</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2">Country</div>
            <div class="input-group-sm col-md-2 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
            <div class="d-flex p-2 mt-2 text-danger">*</div>
        </div>
        <div class="row col-md-12">
            <button type="button" class="btn-sm btn-danger ml-3 mt-1 mb-2">OK ></button>
            <button type="button" class="btn-sm btn-danger ml-3 mt-1 mb-2">Cancel ></button>
        </div>
    </div>
    <a class="btn btn-danger" data-toggle="collapse" href="#more" role="button" aria-expanded="false" aria-controls="filter">
        Add line
    </a>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2 mt-1"><strong>3. Contract details</strong></div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2">Contract</div>
        <div class="form-check d-flex p-2">
            <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
        </div>
        <div class="d-flex p-2">Numbered</div>
        <div class="form-check d-flex p-2 ml-4">
            <input class="form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" value="option1" aria-label="...">
        </div>
        <div class="d-flex p-2">Without number</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2">Number</div>
        <div class="input-group-sm col-md-5 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2">Amount</div>
        <div class="input-group-sm col-md-2 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2">Currency</div>
        <div class="input-group-sm col-md-2 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2"></div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
        <div class="d-flex p-2">Without amount</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2">Sign Date</div>
        <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
        <div class="d-flex p-2 mt-2 text-danger">*</div>
        <div class="mt-2 ml-2">Maturity date</div>
        <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-6 d-flex p-2">
            Comments<br>Attention: It is requred to indicate in this field expected loan repayment periods and expected interest payments in case of registration of a loan provided to a non-resident.
        </div>
        <textarea class="custom-select-sm col-md-6 mt-1 form-control"></textarea>
    </div>
    <form class="col-md-2 mt-3" action="#">
        <input class="text-danger border-danger" type="submit" value="Attachments... >"/>
    </form>
    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Sign >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Reuse >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Print >"/>
        </form>
        <form class="mt-2 ml-3" action="/currency_control_overview">
            <input class="text-danger border-danger" type="submit" value="Exit >"/>
        </form>
    </div>
</div>

</#macro>