<#import "../for_paym_benef/for_paym_benef_table.ftl" as tab>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro for_paym_benef>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-7 pl-5"><strong>BENEFICIARIES ADMINISTRATION FOREIGN</strong></h5>
        <div class="col-md-5 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Name</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2 mt-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
            <input class="d-flex p-2 mt-2" type="checkbox" checked/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public for Clients</strong></small></div>
            <input class="d-flex p-2 mt-2" type="checkbox" checked/>
            <select class="custom-select-sm col-md-7 bg-white d-flex p-2 ml-5">
                <option value="all">All</option>
                <option value="cli1">Cli-1</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>More search criteria</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="ben" data-toggle="collapse" data-target="#filter" onclick="benef()"></div>
        </div>
        <div class="collapse" id="filter">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary bank / BIC</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary account / IBAN</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Select ></strong></small></a>
            </div>
            <div class="mt-1 ml-4 pb-3">
                <a href="/foreign_payments_beneficiary" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Clear ></strong></small></a>
            </div>
        </div>
    </div>
    <div class="d-flex p-2 mt-3">
        <@tab.for_benef_table/>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="mt-1 ml-2 pb-3">
            <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>New ></strong></small></a>
        </div>
        <div class="mt-1 ml-4 pb-3">
            <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Export ></strong></small></a>
        </div>
    </div>
</div>

<script type="text/javascript">
    var more6 = false;
    function benef(){
        if(!more6){
            more6 = true;
            document.getElementById("ben").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            more6 = false;
            document.getElementById("ben").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>

</#macro>