<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/search.ftl" as se>
<#import "../settings/activity_log_table.ftl" as ta>

<#macro set_activity_log>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5 pb-1"><strong>ACTIVITY LOG</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary mt-3"><small><strong>Date/time of action from</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2 mt-3"/>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2 mt-3 ml-3" placeholder="00:00:00"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Date/time of action to</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2 ml-3" placeholder="23:59:59"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="type">
                <option value="all">All</option>
                <option value="cdc">Confirming Document Certificate</option>
                <option value="curbuy">Currency Buy</option>
                <option value="coc">Currency Operations Certificate</option>
                <option value="cursel">Currency Sell</option>
                <option value="cuspaym">Custom Payment</option>
                <option value="domoth">Domestic Other</option>
                <option value="domsta">Domestic Standard</option>
                <option value="fx">FX Order</option>
                <option value="forpaymbank">Foreign Payment for Bank</option>
                <option value="forpaymcurracc">Foreign Payment from Current Account</option>
                <option value="forpaymbank">Foreign Payment from Transit Account</option>
                <option value="interpaym">Interbank Payment</option>
                <option value="opdeal">Open Deal Passport</option>
                <option value="rendeal">Renew Deal Passport</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="acc">
                <option value="all">All</option>
                <option value="acc1">Acc-1</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2" id="amount"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount Currency</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="ml-2 mt-1"><@se.search/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Status</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="status">
                <option value="all">All</option>
                <option value="suc">successful</option>
                <option value="err">error</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Only own logs</strong></small></div>
            <input class="d-flex p-2 mt-2" type="checkbox" checked id="only"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="mt-3 ml-4 pb-3">
                <a href="#" class="btn btn-danger"><small><strong>Search ></strong></small></a>
            </div>
            <div class="mt-3 ml-4 pb-3">
                <a href="/settings_activity_log" class="btn btn-outline-danger"><small><strong>Reset ></strong></small></a>
            </div>
        </div>
    </div>
    <div class="d-flex p-2 mt-3 pb-5">
        <@ta.table/>
    </div>
</div>
</#macro>