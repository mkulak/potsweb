<#import "../for_paym_create/for_cur_control_table.ftl" as cur>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/search.ftl" as se>
<#import "../awesome/info.ftl" as info>
<#import "../awesome/save.ftl" as sav>

<#macro for_trans_acc_ord>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-8 pl-5"><strong>TRANSIT CURRENCY ORDER</strong></h5>
        <div class="col-md-4 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Templates</strong></small></div>
            <select class="custom-select-sm col-md-8 mt-2 bg-white d-flex p-2 ">
                <option selected>Select a Template</option>
                <option value="temp1">Template-1</option>
            </select>
            <div class="d-flex justify-content-end p-2 mt-2 fa">
                <@se.search "1"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Status</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small><strong>New</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Document No.</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "3"/></div>
            <div class="col-md-1 d-flex p-2 ml-2 text-secondary"><small><strong>Date</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Deal Type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 ">
                <option value="bank">Bank</option>
                <option value="central">Central Bank</option>
                <option value="exchange">The exchange</option>
                <option value="interbank">in the interbank parker</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Transit account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 ">
                <option selected>Select an Account</option>
                <option value="acc1">Acc-1</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Account Owner</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>INN</strong></small></div>
            <div class="col-md-4 d-flex p-2"><small>Value</small></div>
            <div class="col-md-2 d-flex p-2 text-secondary"><small><strong>OKPO</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Sender Official</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <input class="d-flex p-2 ml-1 mt-2" type="checkbox"/>
            <div class="d-flex p-2"><small>Set as default</small></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "5"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Phone Official</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <input class="d-flex p-2 ml-1 mt-2" type="checkbox"/>
            <div class="d-flex p-2"><small>Set as default</small></div>
            <div class="d-flex justify-content-end p-2 fa"><@info.fav "6"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>COC Num</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "9"/></div>
            <div class="col-md-1 d-flex p-2 ml-4 text-secondary"><small><strong>Date</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
        </div>
        <hr class="mr-3 ml-3" style="border-top: 1px dotted #8c8b8b;">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>From the total amount of delivered funds</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2" placeholder="0,00"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>To be debited to transit a/c</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" placeholder="0,00" disabled/>
        </div>
        <hr class="mr-3 ml-3" style="border-top: 1px dotted #8c8b8b;">
        <div class="row col-md-12 ml-1">
            <input class="d-flex p-2 mt-2" type="checkbox"/>
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>To CREDIT funds to amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary ml-3"><small><strong>VO Code</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "5"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-1 d-flex p-2 text-secondary ml-3"><small><strong>To a/c</strong></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" checked disabled>
            <div class="col-md-3 d-flex p-2 ml-3"><small>with our bank</small></div>
            <select class="custom-select-sm col-md-3 bg-white d-flex p-2" disabled>
                <option selected>Select an Account</option>
                <option value="acc1">Acc-1</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex p-2 ml-3"><small>with BIC code</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2" disabled/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-1 d-flex p-2 text-secondary ml-3"><small><strong></strong></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" disabled>
            <div class="col-md-3 d-flex p-2 ml-3"><small>with another credit institute</small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary ml-3"><small><strong>Details of the bank to credit curr. funds</strong></small></div>
            <textarea class="custom-select-sm col-md-7 form-control mb-1" disabled></textarea>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary ml-3"><small><strong>Swift code</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "9"/></div>
        </div>
        <hr class="mr-3 ml-3" style="border-top: 1px dotted #8c8b8b;">
        <div class="row col-md-12 ml-1">
            <input class="d-flex p-2 mt-2" type="checkbox"/>
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>To SELL funds to amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary ml-3"><small><strong>VO Code</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "5"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-1 d-flex p-2 text-secondary ml-3"><small><strong>To a/c</strong></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" checked disabled>
            <div class="col-md-3 d-flex p-2 ml-3"><small>with our bank</small></div>
            <select class="custom-select-sm col-md-3 bg-white d-flex p-2" disabled>
                <option selected>Select an Account</option>
                <option value="acc1">Acc-1</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex p-2 ml-3"><small>with BIC code</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2" disabled/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex justify-content-end p-2 fa"><@se.search "5"/></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-1 d-flex p-2 ml-3"><small></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" disabled>
            <div class="col-md-3 d-flex p-2 ml-3"><small>with another credit institute</small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2" disabled/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary ml-3"><small><strong>Details of the bank to credit ruble funds</strong></small></div>
            <textarea class="custom-select-sm col-md-7 form-control mb-1" disabled></textarea>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary ml-3"><small><strong>Sale to be executed</strong></small></div>
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" checked disabled>
            <div class="col-md-3 d-flex p-2 ml-3"><small>at Banks exchange rate</small></div>
        </div>
        <hr class="mr-3 ml-3" style="border-top: 1px dotted #8c8b8b;">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>Comission</strong></small></div>
            <div class="col-md-8 d-flex p-2 text-danger"><small><strong>This commission is applicable ONLY for funds transfer to other bank</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1" checked>
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>to be debitted to our a/c</strong></small></div>
            <select class="custom-select-sm col-md-5 bg-white d-flex p-2">
                <option selected>Select an Account</option>
                <option value="acc1">Acc-1</option>
            </select>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="col-md-1 d-flex p-2 ml-3"><small>with</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2" disabled/>
        </div>
        <div class="row col-md-12 ml-1">
            <input class="d-flex p-2 mt-2 form-check-input position-static ml-2" type="radio" name="blankRadio1" id="blankRadio1">
            <div class="col-md-5 d-flex p-2 text-secondary"><small><strong>to be withdrawed from transaction amount</strong></small></div>
        </div>
        <hr class="mr-3 ml-3" style="border-top: 1px dotted #8c8b8b;">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>Agreement with Bank</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-11 d-flex p-2"><small>Перевод средств соответствует осн. деятельности, Уставу предприятия и действующему вал. законодательству</small></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-5 d-flex p-2 mt-2"><small><strong>Currency Control Information</strong></small></div>
        </div>
        <div class="col-md-12">
            <@cur.cur_control/>
        </div>
        <div class="row">
            <div class="mt-1 ml-4 pb-3">
                <a href="#" class="btn btn-outline-danger ml-2 mt-2" style="border-radius: 8px"><small><strong>Add line ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger mt-2" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-danger"><small><strong>Save Document Template</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-3 ml-2" id="save" data-toggle="collapse" data-target="#template" onclick="save()"></div>
        </div>
        <div class="collapse mt-1" id="template">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
                <@sav.print "2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio3" id="blankRadio3" checked>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio4" id="blankRadio4">
                <div class="d-flex justify-content-end p-2 fa"><@info.fav "4"/></div>
            </div>
        </div>
    </div>
    <div class="mt-3 col-md-12">
        <div class="row">
            <div class="ml-4 pb-3 mt-1">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Save & sign ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & new ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & reuse ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="/foreign_payments_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Exit ></strong></small></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var fil3 = false;
    function save(){
        if(!fil3){
            fil3 = true;
            document.getElementById("save").className = "fas fa-minus-circle text-secondary fa-sm mt-3 ml-2";
        } else {
            fil3 = false;
            document.getElementById("save").className = "fas fa-plus-circle text-secondary fa-sm mt-3 ml-2";
        }
    }
</script>

</#macro>