<#import "../dom_paym_view/dom_paym_filter.ftl" as fi>
<#import "../dom_paym_view/dom_paym_table.ftl" as ta>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/help.ftl" as he>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro dom_paym_view>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5"><strong>FINANCIAL OVERVIEW</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@he.help/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Order Type</strong></small></div>
            <select class="custom-select-sm col-md-3 mt-2 bg-white d-flex p-2 " id="type">
                <option value="all">All</option>
                <option value="dom">Domestic Standard</option>
                <option value="budg">Budgetary Payment</option>
                <option value="cust">Custom Payment</option>
            </select>
            <div class="col-md-2 d-flex p-2 mt-2 text-secondary pl-3"><small><strong>Status</strong></small></div>
            <select class="custom-select-sm col-md-3 bg-white d-flex p-2 mt-2" id="status">
                <option value="all">All</option>
                <option value="tobe_sign">To be signed</option>
                <option value="all_accepted">All Accepted</option>
                <option value="accepted">Accepted</option>
                <option value="no_founds">No funds</option>
                <option value="accepted_wait">Accepted - Wait for linked doc</option>
                <option value="hold_cc">On hold CC</option>
                <option value="no_founds_queue">No funds queue</option>
                <option value="pre_booked">Pre-booked</option>
                <option value="booked">Booked</option>
                <option value="rejected">Rejected</option>
                <option value="p_sign">P.signed</option>
                <option value="sign_process">Signature in process</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Organization</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 " id="org">
                <option value="all">All</option>
                <option value="cli1">Cli-1</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Accounts</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 " id="accs">
                <option value="all">All</option>
                <option value="acc1">Acc-1</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Date from/to</strong></small></div>
            <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" checked>
            <input type="date" class="form-control-sm col-md-2 d-flex p-2"/>
            <input type="date" class="form-control-sm col-md-2 d-flex p-2"/>
            <input class="col-md-1 d-felx p-2 mt-2 ml-2 form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1">
            <div class="d-flex p-2 pr-3"><small>Last days</small></div>
            <select class="custom-select-sm col-md-1 d-flex p-2">
                <option selected>15</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="1">4</option>
                <option value="2">5</option>
                <option value="3">6</option>
                <option value="1">7</option>
                <option value="2">8</option>
                <option value="3">9</option>
                <option value="1">10</option>
                <option value="2">20</option>
                <option value="3">30</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>More search criteria</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="more" data-toggle="collapse" data-target="#filter" onclick="more()"></div>
        </div>
        <div class="collapse" id="filter">
            <@fi.dom_paym_filter/>
        </div>
        <div class="row col-md-12">
            <div class="mt-3 ml-4 pb-3">
                <a href="" class="btn btn-danger" onclick="update()" style="border-radius: 8px"><small><strong>Search ></strong></small></a>
            </div>
            <div class="mt-3 ml-4 pb-3">
                <a href="/domestic_payments_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Reset ></strong></small></a>
            </div>
            <input class="col-md-1 d-flex p-2 mt-4" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex mt-4 mr-3"><small>result max.10000. lines</small></div>
            <input class="col-md-1 d-flex p-2 mt-4" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex mt-4"><small>Show executed orders</small></div>
        </div>
    </div>
    <div class="d-flex p-2 mt-3 pb-5">
        <@ta.dom_paym_table/>
    </div>
    <div class="mt-3 col-md-12">
        <div class="row">
            <div class="mt-1 ml-4 pb-3">
                <a href="/domestic_payments_create" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Create ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="/start_import_payments" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Import ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Delete ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Decline ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Recall ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Reuse ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Check counterparty ></strong></small></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var deb = false;
    function more(){
        if(!deb){
            deb = true;
            document.getElementById("more").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            deb = false;
            document.getElementById("more").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
</#macro>