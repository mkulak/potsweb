<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro card_stat>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5 pb-2"><strong>CARD STATEMENTS</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1 pt-3 pb-3">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Statements for Card no.</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="card">
                <option value="card1">Card-1</option>
                <option value="card2">Card-2</option>
            </select>
        </div>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="col-md-8 d-flex p-2 mt-3"><small><strong>Overview of available statements for selected card:</strong></small></div>
    </div>
    <div class="d-flex p-2 mt-1 pb-5">
        <table class="table-sm table-striped table-hover col-md-12 shadow">
            <thead class="table-primary">
            <tr>
                <th scope="col"><small><b>Statement Date</b></small></th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

</#macro>