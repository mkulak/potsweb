<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/help.ftl" as he>

<#macro set_pw_change>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5 pb-1"><strong>PASSWORD CHANGE</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@he.help/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-11 d-flex p-2 mt-2 mb-1"><small>Your new password must have at least 8 characters, contain at least one lowercase and one uppercase letter, one number/character: 0123456789~!@#$%^&*()_+`-=:;.</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>Old password</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>New password</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong>Retype new password</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2 mb-3"/>
        </div>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="mt-3 ml-4 pb-3">
            <a href="#" class="btn btn-danger"><small><strong>Change Password ></strong></small></a>
        </div>
    </div>
</#macro>