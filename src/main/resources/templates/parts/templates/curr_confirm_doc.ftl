<#macro curr_confirm_doc>
<div class="col-md-12 shadow">
    <h3 class="text-danger">CONFIRMING DOCUMENT</h3>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order status</div>
        <div class="col-md-6 d-flex p-2 mt-1">New</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
        <div class="col-md-6 d-flex p-2 mt-1">Confirming Document</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Confirming Doc No</div>
        <div class="input-group-sm col-md-8 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Confirming Doc Date</div>
        <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Without number</div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Client</div>
        <select class="custom-select-sm col-md-8 mt-2">
            <option selected>Select a client</option>
            <option value="1">Cli1</option>
            <option value="2">Cli2</option>
            <option value="3">Cli3</option>
        </select>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Document Type</div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Select document type</option>
            <option value="1">type1</option>
            <option value="2">type2</option>
            <option value="3">type3</option>
        </select>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
        <div class="input-group-sm col-md-5 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Paperbased no sign</div>
        <input class="ml-3 mt-3" type="checkbox" aria-label="Checkbox for following text input"/>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Amount</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
        <div class="input-group-sm col-md-2 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="col-md-2 d-flex p-2 mt-1">Currency</div>
        <div class="input-group-sm col-md-1 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="d-flex p-2 mt-1 text-danger">*</div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Amount in contr. curr.</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
        <div class="input-group-sm col-md-2 d-flex p-2 ml-4">
            <input type="text" class="form-control"/>
        </div>
        <div class="col-md-2 d-flex p-2 mt-1">Contract Curr.</div>
        <div class="input-group-sm col-md-1 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Deliveries indicate</div>
        <select class="custom-select-sm col-md-3 mt-2">
            <option selected>Select deliveries indicate</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
        </select>
        <div class="input-group-sm col-md-5 d-flex p-2 ml-4">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Valid Until</div>
        <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
            <input type="text" class="form-control">
            <div class="input-group-addon">
                <span class="glyphicon glyphicon-th"></span>
            </div>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2 mt-1">Amount</div>
        <div class="input-group-sm col-md-3 d-flex p-2">
            <input type="text" class="form-control"/>
        </div>
    </div>
    <div class="row col-md-12">
        <div class="col-md-3 d-flex p-2">Comments</div>
        <textarea class="custom-select-sm col-md-8 mt-1 ml-2 form-control"></textarea>
    </div>
    <form class="col-md-2 mt-3" action="#">
        <input class="text-danger border-danger" type="submit" value="Attachments... >"/>
    </form>
    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Sign >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Reuse >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save >"/>
        </form>
        <form class="mt-2 ml-3" action="/currency_control_overview">
            <input class="text-danger border-danger" type="submit" value="Exit >"/>
        </form>
    </div>
</div>
</#macro>