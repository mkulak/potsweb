<#import "../dom_paym_create/beneficiary_filter.ftl" as ben>
<#import "../dom_paym_create/details_filter.ftl" as det>
<#import "../dom_paym_create/one_time_notif.ftl" as no>
<#import "../dom_paym_create/curr_control_table.ftl" as cu>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>
<#import "../awesome/search.ftl" as se>
<#import "../awesome/info.ftl" as info>
<#import "../awesome/save.ftl" as sav>

<#macro dom_paym_create>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5"><strong>DOMESTIC STANDARD</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Templates</strong></small></div>
            <select class="custom-select-sm col-md-8 mt-2 bg-white d-flex p-2 " id="temp">
                <option selected>Select a Template</option>
                <option value="temp1">Template-1</option>
            </select>
            <div class="d-flex justify-content-end p-2 mt-2 fa">
                <@se.search "1"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Order Status</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small><strong>New</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Type</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 " id="type">
                <option value="dom">Domestic Standard</option>
                <option value="budg">Budgetary Payment</option>
                <option value="cust">Custom Payment</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer Account</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 " id="payer">
                <option value="acc1">Acc-1</option>
                <option value="acc2">Acc-2</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Type/Payer</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer INN</strong></small></div>
            <div class="col-md-2 d-flex p-2"><small>Value</small></div>
            <div class="col-md-2 d-flex p-2 text-secondary"><small><strong>Payer KPP</strong></small></div>
            <input type="text" class="form-control-sm col-md-2 d-flex p-2"/>
            <input class="d-flex p-2 ml-3 mt-2" type="checkbox" aria-label="Checkbox for following text input"/>
            <div class="d-flex p-2"><small>Set as default</small></div>
            <div class="d-flex justify-content-end p-2 fa">
                <@info.fav "1"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>BIC</strong></small></div>
            <div class="col-md-2 d-flex p-2"><small>Value</small></div>
            <div class="col-md-2 d-flex p-2 text-secondary"><small><strong>Corr Account</strong></small></div>
            <div class="col-md-4 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payer Bank Name</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Bank Place</strong></small></div>
            <div class="col-md-6 d-flex p-2"><small>Value</small></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2 text-secondary"><small><strong>Beneficiary(Partner)</strong></small></div>
            <select class="custom-select-sm col-md-8 mt-2 bg-white d-flex p-2 " id="temp">
                <option selected>Select a Partner</option>
                <option value="part1">Partner-1</option>
            </select>
            <div class="d-flex justify-content-end p-2 mt-2 fa">
                <@se.search "2"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1 mb-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Beneficiary Account</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 ml-2" id="more1" data-toggle="collapse" data-target="#filter1" onclick="more1()"></div>
        </div>
        <div class="collapse" id="filter1">
            <@ben.benef_filter/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Document No.</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="d-flex p-2 fa">
                <@se.search "3"/>
            </div>
            <div class="col-md-1 d-flex p-2 text-secondary ml-2"><small><strong>Date</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="col-md-1 d-flex p-2 text-danger"><small><strong>*</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Amount</strong></small></div>
            <input type="text" class="form-control-sm col-md-3 d-flex p-2"/>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="col-md-3 d-flex p-2"><small>Value</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Delivery Type</strong></small></div>
            <select class="custom-select-sm col-md-3 bg-white d-flex p-2 " id="deliv">
                <option value="4">4-</option>
                <option value="1">1-internet</option>
                <option value="2">2-email</option>
                <option value="3">3-telegraph</option>
            </select>
            <div class="col-md-2 d-flex p-2 text-secondary pl-5"><small><strong>Priority</strong></small></div>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3"></div>
            <div class="col-md-9 d-flex p-2 text-danger">
                <small>For urgent payments delivery type is "urgent" (including budget and customs payments)</small>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Code</strong></small></div>
            <input type="text" class="form-control-sm col-md-5 d-flex p-2"/>
            <div class="d-flex justify-content-end p-2 fa">
                <@info.fav "2"/>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Payment Details</strong></small></div>
            <textarea class="custom-select-sm col-md-8 form-control"></textarea>
            <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2" id="more2" data-toggle="collapse" data-target="#filter2" onclick="more2()"></div>
        </div>
        <div class="collapse" id="filter2">
            <@det.details_filter/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-5 d-flex p-2 mt-2"><small><strong>Currency Control Information</strong></small></div>
        </div>
        <div class="col-md-12 mt-1">
            <@cu.curr_cont/>
        </div>
        <div class="row">
            <div class="mt-1 ml-4 pb-3">
                <a href="#" class="btn btn-outline-danger ml-2 mt-2" style="border-radius: 8px"><small><strong>Add line ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger mt-2" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3 pb-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>Save Partner</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2" id="more4" data-toggle="collapse" data-target="#filter4" onclick="more4()"></div>
        </div>
        <div class="collapse mt-1" id="filter4">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
                <@sav.print "1"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Partner Group Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio1" id="blankRadio1" checked>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio2" id="blankRadio2">
                <div class="col-md-3 d-flex p-2"><small>Value</small></div>
            </div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>Save Document Template</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2" id="more5" data-toggle="collapse" data-target="#filter5" onclick="more5()"></div>
        </div>
        <div class="collapse mt-1" id="filter5">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Name</strong></small></div>
                <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
                <div class="d-flex p-2 text-danger"><small><strong>*</strong></small></div>
                <@sav.print "2"/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Private</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio3" id="blankRadio3" checked>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Public</strong></small></div>
                <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio4" id="blankRadio4">
                <div class="col-md-3 d-flex p-2"><small>Value</small></div>
            </div>
        </div>
    </div>
    <div class="mt-3 col-md-12">
        <div class="row">
            <div class="ml-4 pb-3 mt-1">
                <a href="#" class="btn btn-danger" style="border-radius: 8px"><small><strong>Save & sign ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & new ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save & reuse ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Save ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="#" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Print ></strong></small></a>
            </div>
            <div class="mt-1 ml-2 pb-3">
                <a href="/domestic_payments_overview" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Exit ></strong></small></a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var fil1 = false;
    function more1(){
        if(!fil1){
            fil1 = true;
            document.getElementById("more1").className = "fas fa-minus-circle text-secondary fa-sm mt-2 ml-2";
        } else {
            fil1 = false;
            document.getElementById("more1").className = "fas fa-plus-circle text-secondary fa-sm mt-2 ml-2";
        }
    }
    var fil2 = false;
    function more2(){
        if(!fil2){
            fil2 = true;
            document.getElementById("more2").className = "fas fa-minus-circle text-secondary fa-sm mt-2";
        } else {
            fil2 = false;
            document.getElementById("more2").className = "fas fa-plus-circle text-secondary fa-sm mt-2";
        }
    }
    var fil4 = false;
    function more4(){
        if(!fil4){
            fil4 = true;
            document.getElementById("more4").className = "fas fa-minus-circle text-secondary fa-sm mt-2";
        } else {
            fil4 = false;
            document.getElementById("more4").className = "fas fa-plus-circle text-secondary fa-sm mt-2";
        }
    }
    var fil5 = false;
    function more5(){
        if(!fil5){
            fil5 = true;
            document.getElementById("more5").className = "fas fa-minus-circle text-secondary fa-sm mt-2";
        } else {
            fil5 = false;
            document.getElementById("more5").className = "fas fa-plus-circle text-secondary fa-sm mt-2";
        }
    }
</script>

</#macro>