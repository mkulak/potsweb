<#import "../cards_history/card_filter.ftl" as fi>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro card_hist>
<div class="shadow pb-3" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5"><strong>CARD HISTORY</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1 mt-2 pt-2">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Chosen Card</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="cards">
                <option value="card1">Card-1</option>
                <option value="card2">Card-2</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>History</strong></small></div>
            <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1" checked>
            <select class="custom-select-sm col-md-3 bg-white d-flex p-2" id="days">
                <option value="15">15</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
            </select>
            <div class="col-md-3 d-flex p-2 ml-2"><small>last days</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>From / To</strong></small></div>
            <input class="col-md-1 d-felx p-2 mt-2 form-check-input position-static" type="radio" name="blankRadio" id="blankRadio1">
            <input type="date" class="form-control-sm col-md-3 d-flex p-2"/>
            <input type="date" class="form-control-sm col-md-3 d-flex p-2 ml-3"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Debit / Credit</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="debcre">
                <option value="all">All</option>
                <option value="incom">Incoming</option>
                <option value="outgo">Outgoing</option>
            </select>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-danger"><small><strong>More search criteria</strong></small></div>
            <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="more" data-toggle="collapse" data-target="#filter" onclick="more()"></div>
        </div>
        <div class="collapse" id="filter">
            <@fi.card_filter/>
        </div>
        <div class="row col-md-7">
            <div class="mt-3 ml-4 pb-3">
                <a href="" class="btn btn-danger" onclick="update()" style="border-radius: 8px"><small><strong>Search ></strong></small></a>
            </div>
            <div class="mt-3 ml-4 pb-3">
                <a href="/cards_history" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Reset ></strong></small></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var more1 = false;
    function more(){
        if(!more1){
            more1 = true;
            document.getElementById("more").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            more1 = false;
            document.getElementById("more").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
</#macro>