<#macro term_depo_new_agree>
<div class="col-md-12 shadow">
    <h4 class="text-danger">ORDER</h4>
    <div class="col-md-12 shadow">
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Status</div>
            <div class="col-md-3 d-flex p-2 mt-1"><strong>New</strong></div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Order type</div>
            <div class="col-md-3 d-flex p-2 mt-1"><strong>Term Deposit</strong></div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">From Account</div>
            <select class="custom-select-sm col-md-8 mt-2">
                <option selected>Select...</option>
                <option value="1">Acc1</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger mt-1">*</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Account Owner</div>
            <div class="col-md-6 d-flex p-2 mt-1">"ЕВРОП АССИСТАНС СНГ"</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">According to</div>
            <select class="custom-select-sm col-md-8 mt-2">
                <option selected>Select...</option>
                <option value="1">Common deposit conditions agreement</option>
                <option value="2">Complex banking service agreement</option>
            </select>
            <div class="col-md-1 d-flex p-2 text-danger mt-1">*</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Document No.</div>
            <div class="input-group-sm col-md-3 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
            <div class="col-md-3 d-flex p-2 mt-1">Document Date</div>
            <div class="input-group-sm date col-md-2 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Amount</div>
            <div class="input-group-sm col-md-3 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
            <div class="d-flex p-2 text-danger mt-1">*</div>
            <div class="col-md-2 d-flex p-2 mt-1">value</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Start Date</div>
            <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Maturity Date</div>
            <div class="input-group-sm date col-md-3 d-flex p-2" data-provide="datepicker">
                <input type="text" class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
            <div class="col-md-1 d-flex p-2 text-danger mt-1">*</div>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1">Interest Rate</div>
            <div class="input-group-sm col-md-3 d-flex p-2">
                <input type="text" class="form-control"/>
            </div>
            <div class="d-flex p-2 text-danger mt-1">*</div>
            <form class="mt-2 ml-3" action="#">
                <input class="text-danger border-danger" type="submit" value="Request interest rate >"/>
            </form>
        </div>
        <div class="row col-md-12">
            <div class="col-md-3 d-flex p-2 mt-1"></div>
            <div class="col-md-9 d-flex p-2 mt-1 text-danger"><small>Attention, it is required to request the new deposit rate in case you change currency, amount or period of deposit.</small></div>
        </div>
    </div>
    <div class="row col-md-12">
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save & Sign >"/>
        </form>
        <form class="mt-2 ml-3" action="#">
            <input class="text-danger border-danger" type="submit" value="Save >"/>
        </form>
        <form class="mt-2 ml-3 mb-2" action="/term_deposit_overview">
            <input class="text-danger border-danger" type="submit" value="Exit >"/>
        </form>
    </div>
</div>

</#macro>