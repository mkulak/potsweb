<#macro set_my_menu>
<div class="col-md-12 shadow">
    <h4 class="text-danger">MY MENU</h4>
    <div class="row col-md-12">
        <div class="col-md-1 d-flex p-2 mt-1">User: </div>
        <div class="col-md-3 d-flex p-2 mt-1">value value</div>
    </div>
    <table class="table-sm table-striped col-md-12 mt-3">
        <thead class="table-primary">
        <tr>
            <th scope="col"></th>
            <th scope="col"><small><b>My Startpage</b></small></th>
            <th scope="col"><small><b>Portlet</small></th>
            <th scope="col"><small><b>Navigational Path</b></small></th>
            <th scope="col"><small><b>Actions</b></small></th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    <div class="row col-md-12">
        <select class="custom-select-sm col-md-2 mt-3">
            <option selected>Select action...</option>
            <option value="1">Delete selected</option>
            <option value="1">Set/unset Startpage</option>
        </select>
    </div>
    <div class="mt-3"><small>The function selected as "Startpage" will be the first one after login!<br>
        In case the selected product does not include the selected function, the default behavior will be applied within this product!</small></div>
</div>

</#macro>