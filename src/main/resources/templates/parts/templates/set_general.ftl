<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro set_general>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5 pb-1"><strong>GENERAL SETTINGS</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-1" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2"><small><strong>Regional Settings</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Date Format</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="date">
                <option selected>Select a value</option>
                <option value="op1">DD.MM.YY</option>
                <option value="op2">DD.MM.YYYY</option>
                <option value="op3">MM/DD/YY</option>
                <option value="op4">MM/DD/YYYY</option>
                <option value="op5">YY.MM.DD</option>
                <option value="op6">YYYY.MM.DD</option>
            </select>
            <div class="text-danger d-flex p-2"><small>*</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Number Format</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2" id="number">
                <option selected>Select a value</option>
                <option value="1">Decimal,(comma) and thousands.(period)(e.g.5.000,000)</option>
                <option value="2">Decimal.(period) and thousands,(comma)(e.g.5,000.000)</option>
                <option value="3">Decimal.(period) and thousands (space)(e.g.5 000.000)</option>
            </select>
            <div class="text-danger d-flex p-2"><small>*</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Character Encoding</strong></small></div>
            <select class="custom-select-sm col-md-8 bg-white d-flex p-2 mb-3" id="encoding">
                <option value="utf">Universal(UTF-8)</option>
                <option value="cp">Windows(CP1251)</option>
            </select>
            <div class="text-danger d-flex p-2"><small>*</small></div>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2"><small><strong>Contact</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong></strong></small></div>
            <div class="col-md-1 d-flex p-2"><small>Country</small></div>
            <div class="col-md-1 d-flex p-2"></div>
            <div class="col-md-1 d-flex p-2"><small>Area</small></div>
            <div class="col-md-1 d-flex p-2"></div>
            <div class="col-md-3 d-flex p-2"><small>Number</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Phone number</strong></small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2" placeholder="+7"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-4 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Mobile number</strong></small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2" placeholder="+43"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-4 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Fax number</strong></small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-4 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Email address</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2 mb-3"/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2"><small><strong>SMS Token</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2"><small><strong></strong></small></div>
            <div class="col-md-1 d-flex p-2"><small>Country</small></div>
            <div class="col-md-1 d-flex p-2"></div>
            <div class="col-md-1 d-flex p-2"><small>Area</small></div>
            <div class="col-md-1 d-flex p-2"></div>
            <div class="col-md-3 d-flex p-2"><small>Number</small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>OTP Phone number</strong></small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2" placeholder="+7"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-1 d-flex p-2"/>
            <div class="col-md-1 d-flex justify-content-center p-2"><small>/</small></div>
            <input type="text" class="form-control-sm col-md-4 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Remainig SMS</strong></small></div>
            <span class="col-md-1 d-flex p-2"><small>null</small></span>
            <span class="col-md-3 d-flex p-2"><small>from actual set (100 SMS)</small></span>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Nr. of set</strong></small></div>
            <span class="col-md-1 d-flex p-2"><small>null</small></span>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mt-2"><small><strong>Security Question & Answer</strong></small></div>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Security Question</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2"/>
        </div>
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Security Answer</strong></small></div>
            <input type="text" class="form-control-sm col-md-8 d-flex p-2 mb-3"/>
        </div>
    </div>
    <div class="shadow mr-3 ml-3 mt-3" style="border-radius: 30px">
        <div class="row col-md-12 ml-1">
            <div class="col-md-3 d-flex p-2 mb-2 mt-2"><small><strong>Login downgrade</strong></small></div>
            <input class="col-md-1 form-check-input position-static d-flex p-2 mt-3" name="blankRadio" id="blankRadio1" type="radio" checked/>
            <small class="col-md-1 d-flex p-2 mb-2 mt-2">Yes</small>
            <input class="col-md-1 form-check-input position-static d-flex p-2 mt-3" name="blankRadio" id="blankRadio1" type="radio"/>
            <small class="col-md-1 d-flex p-2 mb-2 mt-2">No</small>
        </div>
    </div>
    <div class="row col-md-12 ml-1">
        <div class="mt-3 ml-4 pb-3">
            <a href="#" class="btn btn-danger"><small><strong>Save ></strong></small></a>
        </div>
    </div>
</div>
</#macro>