<#import "../acc_overview_tables/curr_acc_table.ftl" as cu>
<#import "../acc_overview_tables/debit_card_table.ftl" as de>
<#import "../acc_overview_tables/credit_card_table.ftl" as cr>
<#import "../acc_overview_tables/term_deposit_table.ftl" as te>
<#import "../acc_overview_tables/other_acc_table.ftl" as oth>
<#import "../acc_overview_tables/balance_history.ftl" as bal>
<#import "../acc_overview_tables/fin_overview_table.ftl" as fi>
<#import "../awesome/favorites.ftl" as fa>
<#import "../awesome/help.ftl" as he>
<#import "../awesome/pdf.ftl" as pdf>
<#import "../awesome/print.ftl" as pr>

<#macro acc_view>
<div class="shadow" style="border-radius: 30px">
    <div class="row">
        <h5 class="text-danger mt-3 col-md-5 pl-5"><strong>FINANCIAL OVERVIEW</strong></h5>
        <div class="col-md-7 d-flex justify-content-end p-2 mt-2 fa">
            <@fa.fav/>
            <@he.help/>
            <@pdf.pdf/>
            <@pr.print/>
        </div>
    </div>
    <ul class="nav nav-tabs pl-4 pr-4 mt-1">
        <li class="nav-item">
            <a class="nav-link active text-secondary"  data-toggle="tab" href="#accounts"><small>Accounts Overview</small></a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-secondary" data-toggle="tab" href="#financial"><small>Financial Overview</small></a>
        </li>
    </ul>
    <div class="tab-content pl-2 pr-2">
        <div id="accounts" class="tab-pane fade show active">
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary mt-3"><small><strong>Current Accounts</strong></small></div>
                <div class="fas fa-plus-circle text-secondary fa-sm mt-4 col-md-1" id="cur" data-toggle="collapse" data-target="#curr_accs" onclick="toggleacc()"></div>
            </div>
            <div id="curr_accs" class="collapse col-md-12">
                <@cu.curr_acc/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Debit Cards</strong></small></div>
                <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="deb" data-toggle="collapse" data-target="#debit_cards" onclick="toggledeb()"></div>
            </div>
            <div id="debit_cards" class="collapse col-md-12">
                <@de.debit_card/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Credit Cards</strong></small></div>
                <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="cre" data-toggle="collapse" data-target="#credit_cards" onclick="togglecre()"></div>
            </div>
            <div id="credit_cards" class="collapse col-md-12">
                <@cr.credit_card/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Term Deposit Accounts</strong></small></div>
                <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="depo" data-toggle="collapse" data-target="#term_depo" onclick="toggledepo()"></div>
            </div>
            <div id="term_depo" class="collapse col-md-12">
                <@te.term_depo/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Other Accounts</strong></small></div>
                <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="oth" data-toggle="collapse" data-target="#other_acc" onclick="toggleoth()"></div>
            </div>
            <div id="other_acc" class="collapse col-md-12">
                <@oth.credit_card/>
            </div>
            <div class="row col-md-12 ml-1">
                <div class="col-md-3 d-flex p-2 text-secondary"><small><strong>Balance History</strong></small></div>
                <div class="fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1" id="his" data-toggle="collapse" data-target="#balance" onclick="togglehis()"></div>
            </div>
            <div id="balance" class="collapse">
                <@bal.credit_card/>
            </div>
            <div class="mt-3 ml-4 pb-3">
                <a href="/settings_interface" class="btn btn-outline-danger" style="border-radius: 8px"><small><strong>Banking Settings ></strong></small></a>
            </div>
        </div>
        <div id="financial" class="tab-pane fade">
            <div class="shadow ml-1 mr-1 mt-3" style="border-radius: 30px">
                <div class="row col-md-12 ml-1 pt-3">
                    <div class="col-md-3 d-flex p-2 mt-1 text-secondary"><small><strong>Organization</strong></small></div>
                    <select class="custom-select-sm col-md-8 mt-1 bg-white" id="org">
                        <option value="all">All</option>
                        <option value="1">Cli1</option>
                    </select>
                </div>
                <div class="row col-md-12 ml-1">
                    <div class="col-md-3 d-flex p-2 mt-1 text-secondary"><small><strong>Account Type</strong></small></div>
                    <select class="custom-select-sm col-md-8 mt-1 bg-white" id="acc_type">
                        <option value="all">All</option>
                        <option value="curr">Current</option>
                        <option value="deposit">Term deposit</option>
                        <option value="other">Other</option>
                        <option value="external">External</option>
                    </select>
                </div>
                <div class="row col-md-12 ml-1">
                    <div class="col-md-3 d-flex p-2 mt-1 text-secondary"><small><strong>Currency</strong></small></div>
                    <select class="custom-select-sm col-md-8 mt-1 bg-white" id="currency">
                        <option value="all">All</option>
                        <option value="rur">RUR Russian ruble</option>
                    </select>
                </div>
                <div class="row col-md-12 ml-1">
                    <div class="col-md-3 d-flex p-2 mt-1 text-secondary"><small><strong>Display in currency</strong></small></div>
                    <select class="custom-select-sm col-md-8 mt-1 bg-white" id="display">
                        <option value="acc">Account Currency</option>
                        <option value="rur">RUR Russian ruble</option>
                    </select>
                </div>
                <div class="row col-md-7">
                    <div class="mt-3 ml-4 pb-3">
                        <a href="" class="btn btn-danger" onclick="update()" style="border-radius: 8px"><small><strong>Update ></strong></small></a>
                    </div>
                    <div class="mt-3 ml-4 pb-3">
                        <a href="" class="btn btn-outline-danger" onclick="reset()" style="border-radius: 8px"><small><strong>Reset ></strong></small></a>
                    </div>
                </div>
            </div>
            <div class="d-flex p-2 mt-3">
                <@fi.fin_overview/>
            </div>
            <div class="mt-3 ml-4 pb-3">
                <a href="" class="btn btn-danger" onclick="download()" style="border-radius: 8px"><small><strong>Export ></strong></small></a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var togacc = false;
    function toggleacc(){
        if(!togacc){
            togacc = true;
            document.getElementById("cur").className = "fas fa-minus-circle text-secondary fa-sm mt-4 col-md-1";
        } else {
            togacc = false;
            document.getElementById("cur").className = "fas fa-plus-circle text-secondary fa-sm mt-4 col-md-1";
        }
    }
</script>
<script type="text/javascript">
    var togdeb = false;
    function toggledeb(){
        if(!togdeb){
            togdeb = true;
            document.getElementById("deb").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            togdeb = false;
            document.getElementById("deb").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
<script type="text/javascript">
    var togcre = false;
    function togglecre(){
        if(!togcre){
            togcre = true;
            document.getElementById("cre").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            togcre = false;
            document.getElementById("cre").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
<script type="text/javascript">
    var togdepo = false;
    function toggledepo(){
        if(!togdepo){
            togdepo = true;
            document.getElementById("depo").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            togdepo = false;
            document.getElementById("depo").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
<script type="text/javascript">
    var togoth = false;
    function toggleoth(){
        if(!togoth){
            togoth = true;
            document.getElementById("oth").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            togoth = false;
            document.getElementById("oth").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
<script type="text/javascript">
    var toghis = false;
    function togglehis(){
        if(!toghis){
            toghis = true;
            document.getElementById("his").className = "fas fa-minus-circle text-secondary fa-sm mt-2 col-md-1";
        } else {
            toghis = false;
            document.getElementById("his").className = "fas fa-plus-circle text-secondary fa-sm mt-2 col-md-1";
        }
    }
</script>
<script type="text/javascript">
    function update() {

    }

</script>


</#macro>
