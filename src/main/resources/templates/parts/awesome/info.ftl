<#macro fav info>
<a href="#">
    <span
            class="fas fa-info-circle text-secondary pr-1"
            data-toggle="tooltip"
            data-placement="bottom"
            <#if info=="1">
                title="Set manually entered KPP as default KPP for this payer account"
            <#elseif info=="2">
                title="Since 31.01.2014 'Code' is mandatory for tax payments. If UNI-code is not available, the field should be filled in with value '0'"
            <#elseif info=="3">
                title="Set manually selected VAT Calc type as default VAT Calc for this payer account."
            <#elseif info=="4">
                title="The template of transfer order can be saved as 'private'(visible is only for the creator) or it is possible share ot with users of selected client."
            <#elseif info="5">
                title="Set manually entered sender official as default sender official for this client."
            <#elseif info="6">
                title="Set manually entered phone official as default phone official for this client."
            <#elseif  info="7">
                title="This application should be valid no more than 7 calendar days"
            <#elseif info="8">
                title="Refresh account balance"
            <#elseif info="9">
                title=""
            <#elseif info=="10">
                title="Checksum - this field concerns a sum of amounts of transactions in package regardless the currency of the amount"
            <#elseif info=="11">
                title="Package details - full text searching in the columns 'File name','Description' and 'Digest'(the control word)"
            <#elseif info="12">
                title="In the field 'Format structure' shoose the type of imported file"
            </#if>
    >
    </span>
</a>
</#macro>