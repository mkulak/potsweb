<#macro pdf>
<a href="#">
    <span class="fas fa-file-pdf text-secondary pr-3" data-toggle="tooltip" data-placement="bottom" title="View PDF"></span>
</a>
</#macro>