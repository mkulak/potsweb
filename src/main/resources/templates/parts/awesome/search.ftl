<#macro search param>
<a href="#">
    <span
            class="fas fa-search text-secondary"
            data-toggle="tooltip"
            data-placement="bottom"
            <#if param=="1">
                title="Search for Template"
            <#elseif param=="2">
                title="Search for Partner"
            <#elseif param=="3">
                title="Check Document Number"
            <#elseif param=="4">
                title=""
            <#elseif param=="5">
                title="Check SWIFT / BIC"
            <#elseif param=="6">
                title="Search for Country"
            <#elseif param=="7">
                title="Search for Country Code"
            <#elseif param=="8">
                title="Search for Currency"
            </#if>
    >
    </span>
</a>
</#macro>