<#macro home param>
<a href="#">
    <span
            class="fas fa-home text-secondary"
            data-toggle="tooltip"
            data-placement="bottom"
            <#if param=="1">
                title="Unicredit as Beneficiary"
            </#if>
    >
    </span>
</a>
</#macro>