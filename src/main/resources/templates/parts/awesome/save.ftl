<#macro print param>
<a href="#">
    <span
            class="fas fa-save text-secondary mt-2"
            data-toggle="tooltip"
            data-placement="bottom"
            <#if param=="1">
                title="Save Partner Template"
            <#elseif param=="2">
                title="Save Template"
            </#if>
    >
    </span>

</a>
</#macro>