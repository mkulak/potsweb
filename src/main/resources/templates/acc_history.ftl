<#import "parts/templates/common.ftl" as c>
<#import "parts/navbar/navbar.ftl" as n>
<#import "parts/templates/account_history.ftl" as hi>

<@c.page>
<div class="row">
    <div class="col-sm-2 col-md-3 col-lg-3"><@n.navbar/></div>
    <div class="col-sm-10 col-md-9 col-lg-9"><@hi.history/></div>
</div>
</@c.page>
