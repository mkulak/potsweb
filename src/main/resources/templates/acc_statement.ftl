<#import "parts/templates/common.ftl" as c>
<#import "parts/templates/accounts_statements.ftl" as st>
<#import "parts/navbar/navbar.ftl" as n>

<@c.page>
<div class="row">
    <div class="col-sm-2 col-md-3 col-lg-3"><@n.navbar/></div>
    <div class="col-sm-10 col-md-9 col-lg-9"><@st.stat/></div>
</div>
</@c.page>
